<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Schedule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->tinyInteger('whole_day')->default(0);

            $table->integer('schedule_status_id')->unsigned();
            $table->integer('timeframe_id')->unsigned()->default(0);
            $table->integer('customer_id')->unsigned();
            $table->integer('service_id')->unsigned();
            $table->integer('location_id')->unsigned()->default(0);
            $table->integer('unit_id')->unsigned()->default(0);

            $table->integer('provider_id')->unsigned()->default(0);
            $table->integer('invoice_id')->unsigned()->default(0);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule');
    }
}
