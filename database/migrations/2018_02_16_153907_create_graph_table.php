<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGraphTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('graph', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('relationship_type_id');
            $table->integer('source_id');
            $table->integer('target_id');

            $table->timestamps();
            $table->softDeletes();

            $table->unique(['relationship_type_id', 'source_id', 'target_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('graph');
    }
}
