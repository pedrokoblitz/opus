<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->id();
            $table->integer('app_id')->unsigned();

            $table->integer('product_id')->unsigned()->default(0);
            
            $table->integer('fixed_discount')->default(0);
            $table->integer('percent_discount')->default(0);
            $table->tinyInteger('free_shipping')->default(0);

            $table->string('ref_code');
            $table->string('name')->nullable();
            $table->string('description')->nullable();

            $table->dateTime('begins')->nullable();
            $table->dateTime('expires')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
