<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();

            $table->integer('app_id')->unsigned();
            $table->integer('type_id')->unsigned();

            $table->tinyInteger('activity')->default(1);

            $table->string('name')->default("");
            $table->integer('stock_keeping')->default(1);

            $table->text('keywords')->nullable();
            $table->text('meta_tag_description')->nullable();

            $table->bigInteger('sku')->default(0);
            $table->integer('cubicweight')->default(0);
            $table->integer('weight')->default(0);
            $table->integer('height')->default(0);
            $table->integer('length')->default(0);
            $table->integer('width')->default(0);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
