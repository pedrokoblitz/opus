<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('node_id');
            
            $table->string('street')->default('');
            $table->string('number')->default('');
            $table->string('address_line')->default('');
            $table->string('district')->default('');
            $table->string('city')->default('');
            $table->string('province')->default('');
            $table->string('country')->default('');
            $table->string('zipcode')->default('');
            $table->double('latitude')->default(0);
            $table->double('longitude')->default(0);
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
