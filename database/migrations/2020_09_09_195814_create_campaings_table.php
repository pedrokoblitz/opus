<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampaingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaings', function (Blueprint $table) {
            $table->id();
            $table->integer('app_id')->unsigned();
            $table->integer('type_id')->default(0)->unsigned();
            $table->integer('node_id')->default(0)->unsigned();
            $table->integer('term_id')->default(0)->unsigned();
            $table->string('name');
            $table->string('slug');
            $table->dateTime('last_sent')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaings');
    }
}
