<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UserTracking extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer_tracking', function (Blueprint $table) {
            $table->id();
            $table->string('user_token')->default('');
            $table->integer('lead_id')->default(0)->unsigned();
            $table->integer('person_id')->default(0)->unsigned();
            $table->integer('campaign_id')->default(0)->unsigned();
            $table->tinyInteger('is_confirmed')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer_tracking');
    }
}
