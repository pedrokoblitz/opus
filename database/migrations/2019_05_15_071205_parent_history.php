<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ParentHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parent_history', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('old_parent_id');
            $table->integer('new_parent_id');

            $table->timestamps();

            $table->unique(['new_parent_id', 'old_parent_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parent_history');
    }
}
