<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();

            $table->integer('app_id')->unsigned();
            $table->string('ref_code', 64);

            $table->integer('shipping_status_id')->default(1)->unsigned();
            $table->integer('payment_status_id')->default(1)->unsigned();

            $table->integer('customer_id')->unsigned()->default(0);
            $table->integer('shipping_address_id')->unsigned()->default(0);
            $table->integer('coupon_id')->unsigned()->default(0);

            $table->integer('value')->default(0);
            $table->integer('shipping')->default(0);
            $table->integer('discount')->default(0);
            $table->integer('total')->default(0);

            $table->string('receiver')->nullable();
            $table->string('payment_tracking')->nullable();
            $table->string('shipping_tracking')->nullable();

            $table->dateTime('shipped')->nullable();
            $table->dateTime('expected')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
