<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PersonActions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('person_history', function (Blueprint $table) {
            $table->id();
            
            $table->integer('type_id')->unsigned();
            $table->integer('app_id')->unsigned();
            $table->integer('person_id')->unsigned()->default(0);
            $table->integer('campaign_id')->unsigned()->default(0);
            $table->string('user_identifier', 32)->nullable();

            $table->softDeletes();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('person_history');        
    }
}
