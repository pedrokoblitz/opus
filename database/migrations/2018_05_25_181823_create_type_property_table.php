<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypePropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('types_properties_relationships', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('type_id');
            $table->integer('property_id');
            $table->unique(['type_id', 'property_id']);
        });
    }
/*
 */

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('types_properties_relationships');
    }
}
