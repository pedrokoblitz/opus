<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGraphRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('relationship_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('source_type_id');
            $table->integer('target_type_id');
            $table->string('name');
            $table->string('slug')->unique();

            $table->unique(['source_type_id', 'target_type_id', 'name']);
        });
    }
/*
 */

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('relationship_types');
    }
}
