<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prices', function (Blueprint $table) {
            $table->id();
            $table->integer('product_id')->unsigned();
            $table->integer('price')->default(1);
            $table->tinyInteger('activity')->default(1);
            $table->tinyInteger('default')->default(1);

            $table->integer('fixed_discount')->default(0);
            $table->integer('percent_discount')->default(0);
            $table->tinyInteger('free_shipping')->default(0);

            $table->dateTime('begins')->nullable();
            $table->dateTime('expires')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prices');
    }
}
