<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Addresses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name', 32);
            $table->integer('user_id')->unsigned()->default(0);

            $table->string('street', 32);
            $table->string('number', 8);
            $table->string('address_line');
            $table->string('district', 16);
            $table->string('city', 16);
            $table->string('province', 2);
            $table->string('zipcode', 10);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
