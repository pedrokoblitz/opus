<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->id();

            $table->string('email', 128)->nullable();

            $table->string('name', 128)->nullable();
            $table->string('phone', 32)->nullable();
            
            $table->string('cpf', 32)->nullable();
            $table->string('cnh', 32)->nullable();
            $table->string('id_card', 32)->nullable();

            $table->string('instagram')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('linkedin')->nullable();
            
            $table->date('birthdate')->nullable();

            $table->dateTime('optin')->nullable();
            $table->dateTime('optout')->nullable();
            $table->dateTime('last_campaing_sent')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
