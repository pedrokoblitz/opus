<?php

use Faker\Factory as Faker;

trait FakerTrait
{
    public function getFaker()
    {
        $faker = Faker::create();

        $faker->addProvider(new \Faker\Provider\Internet($faker));
        $faker->addProvider(new \Faker\Provider\File($faker));
        $faker->addProvider(new \Faker\Provider\pt_BR\Address($faker));
        $faker->addProvider(new \Faker\Provider\pt_BR\Person($faker));
        
        return $faker;
    }

    public function makeAddress()
    {
        return [
            'node_id' => $i,
            'width' => 0,
            'height' => 0,
            'filepath' => '/uploads/',
            'filename' => $faker->file('/', '/tmp', false),
            'mimetype' => $faker->mimeType,
        ];
    }

    public function makeFile($node_id)
    {
        return [
            'node_id' => $i,
            'width' => 0,
            'height' => 0,
            'filepath' => '/uploads/',
            'filename' => $faker->file('/', '/tmp', false),
            'mimetype' => $faker->mimeType,
        ];
    }

    public function makeContent($node_id)
    {
        return [
            'node_id' => $node_id,
            'slug' => $faker->slug,
            'title' => $faker->title,
            'subtitle' => $faker->title,
            'tagline' => $faker->title,
            'description' => $faker->text,
            'excerpt' => $faker->text,
            'body' => $faker->text,
        ];
    }

    public function makeResource($node_id)
    {
        return [
            'node_id' => $node_id,
            'url' => $faker->url,
            'embed' => '',
        ];
    }

    public function makeNode($user_id)
    {
        
    }
}
