<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('api')->middleware(['cors'])->group(function () {
    Route::post('register', 'UserAPIController@store');
    Route::post('auth', 'AuthAPIController@auth');

    Route::middleware(['auth'])->group(function () {
        Route::get('/', 'AppDataAPIController@app');
        Route::get('me', 'AppDataAPIController@me');
        Route::get('me/activity', 'AppDataAPIController@activity');
    });

    Route::prefix('node')->middleware(['typing'])->group(function () {
        Route::post('/{type}', 'NodeAPIController@store');
        Route::put('/{type}/{id}', 'NodeAPIController@update');
        Route::delete('/{type}/{id}', 'NodeAPIController@destroy');
        Route::get('/{type}', 'NodeAPIController@index');
        Route::get('/{type}/{id}', 'NodeAPIController@show');

        Route::post('/{id}/tag', 'InteractionAPIController@tag');
        Route::delete('/{id}/untag', 'InteractionAPIController@untag');

        Route::post('/{id}/comment', 'InteractionAPIController@comment');
        Route::delete('/{id}/uncomment/{comment_id}', 'InteractionAPIController@uncomment');
        
        Route::post('/{id}/vote', 'InteractionAPIController@vote');
        Route::delete('/{id}/unvote', 'InteractionAPIController@unvote');
        
        Route::post('/{id}/like', 'InteractionAPIController@like');
        Route::delete('/{id}/unlike', 'InteractionAPIController@unlike');

        Route::post('/{id}/graph', 'InteractionAPIController@graph');
        Route::delete('/{id}/ungraph/{target_id}', 'InteractionAPIController@ungraph');
    });

    Route::middleware([])->group(function () {
        Route::get('admin', 'AppDataAPIController@admin');

        Route::prefix('type')->group(function () {
            Route::post('/', 'TypeAPIController@store');
            Route::put('/{id}', 'TypeAPIController@update');
            Route::delete('/{id}', 'TypeAPIController@destroy');
            Route::get('/', 'TypeAPIController@index');
            Route::get('/{slug}/name', 'TypeAPIController@showBySlug');
            Route::get('/{id}', 'TypeAPIController@show');
        });

        Route::prefix('user')->group(function () {
            Route::post('/', 'UserAPIController@store');
            Route::put('/{id}', 'UserAPIController@update');
            Route::delete('/{id}', 'UserAPIController@destroy');
            Route::get('/', 'UserAPIController@index');
            Route::get('/{id}', 'UserAPIController@show');
        });

        Route::prefix('role')->group(function () {
            Route::post('/', 'RoleAPIController@store');
            Route::put('/{id}', 'RoleAPIController@update');
            Route::delete('/{id}', 'RoleAPIController@destroy');
            Route::get('/', 'RoleAPIController@index');
            Route::get('/{id}', 'RoleAPIController@show');
        });

        Route::prefix('app')->group(function () {
            Route::post('/', 'AppAPIController@store');
            Route::put('/{id}', 'AppAPIController@update');
            Route::delete('/{id}', 'AppAPIController@destroy');
            Route::get('/', 'AppAPIController@index');
            Route::get('/{id}', 'AppAPIController@show');
        });

        Route::prefix('config')->group(function () {
            Route::post('/', 'ConfigAPIController@store');
            Route::put('/', 'ConfigAPIController@update'); // batch update
        });
    });
});
