
# Installation

## Create an env file and add your database credentials
cp .env.example .env

## Generate app key
php artisan key:generate

## Download dependencies
composer install

## Create database with sample app
php artisan migrate
