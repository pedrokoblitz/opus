<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class AdminSingleNodeCriteria implements CriteriaInterface
{
    private $parent_id;
    private $type;
    private $status;
    private $user;
    private $typing;

    public function __construct($user, $input)
    {
        $this->user = $user;

        if (isset($input['parent_id'])) {
            $this->parent_id = $input['parent_id'];
        }

        if (isset($input['status'])) {
            $this->status = $input['status'];
        }

        if (isset($input['type'])) {
            $this->type = $input['type'];
        }
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $filters = [];

        if ($this->parent_id) {
            $filters['parent_id'] = $this->parent_id;
        }

        if ($this->status) {
            $filters['status'] = $this->status;
        }

        if ($this->type !== null) {
            $filters['type_id'] = $this->type->id;
            $properties = $this->type
                ->properties
                ->filter(function ($item) {
                    return $item->loads_from_model;
                })
                ->map(function ($item) {
                    return $item->slug;
                })
                ->toArray();
        }

        $properties[] = 'children';
        $properties[] = 'children.type';
        $properties[] = 'parent';
        $properties[] = 'parent.type';
        $properties[] = 'siblings';
        $properties[] = 'siblings.type';

        $properties[] = 'type';
        $properties[] = 'type.allowedChildren';
        $properties[] = 'type.properties';
        $properties[] = 'type.statuses';
        $properties[] = 'status';

        $result = $model
            ->with($properties)
            ->where($filters);

        return $result;
    }
}
