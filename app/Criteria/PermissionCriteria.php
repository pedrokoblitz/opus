<?php

namespace App\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

class PermissionCriteria implements CriteriaInterface
{
    public function __construct($user, $request, $action)
    {
        $this->typeId = $request['type']->id;
        $this->user = $user;
        $this->action = $action;
    }

    public function apply($model, RepositoryInterface $repository)
    {
        $roleIds = $this->user->roles->map(function ($item) {
            return $item->id;
        })->toArray();
        $filters = [];
        $filters['type_id'] = $this->typeId;
        $filters['action'] = $this->action;
        $result = $model->where($filters)->whereIn('role_id', $roleIds);
            
        return $result;
    }
}
