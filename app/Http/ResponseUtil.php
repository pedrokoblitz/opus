<?php

namespace App\Http;

class ResponseUtil
{
    public static function makeResponse($message, $data, $last_sync_timestamp = [])
    {
        $res = [
            'success' => true,
            'data'    => $data,
            'message' => $message,
        ];

        if (!empty($last_sync_timestamp)) {
            $res['last_sync_timestamp'] = $last_sync_timestamp;
        }
        
        return $res;
    }

    public static function makeError($message, array $data = [])
    {
        $res = [
            'success' => false,
            'message' => $message,
        ];

        if (!empty($data)) {
            $res['data'] = $data;
        }

        return $res;
    }
}
