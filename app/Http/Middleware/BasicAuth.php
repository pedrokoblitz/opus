<?php

namespace App\Http\Middleware;

use Closure;
use Cookie;

class BasicAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $key = '3468c961b2187fda6e13dc60ba72a1f0';
        $secret = '14fcc6b67ee65e45d7ab9e8368ee9952';

        header('Cache-Control: no-cache, must-revalidate, max-age=0');
        
        $has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));
        
        $is_not_authenticated = (
            !$has_supplied_credentials ||
            $_SERVER['PHP_AUTH_USER'] != $key ||
            $_SERVER['PHP_AUTH_PW']   != $secret
        );

        if ($is_not_authenticated) {
            header('HTTP/1.1 401 Authorization Required');
            header('WWW-Authenticate: Basic realm="Access denied"');
            exit;
        }

        return $next($request);
    }
}
