<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\Person;
use App\Models\Campaign;
use App\Models\UserTracking;
use Cookie;
use Log;

class CookieTracker
{
    private $repository;

    public function handle($request, Closure $next)
    {
        $response = $next($request);
        $appId = env('APP_ID');

        /*
            procura identificador do usuario nos cookies
         */

        $customerCookie = Cookie::get('customer');

        if (!$customerCookie) {
            $customerCookie = md5('customer-' . microtime());
        }

        /*
            carrega registro de rastreamento
         */
        $record = UserTracking::firstOrCreate(
            ['identifier', $cookie],
            ['identifier', $cookie]
        )->first();


        /*
            se nao tem identificador cria registro de pessoa
         */
        
        if (!$record->person_id) {
            $person = new Person();
            $person->save();
            $record->person_id = $person->id
        }

        $record->user_identifier = $customerCookie;
        $record->save();

        $cookies = [
            'customer' => $customerCookie,
        ];

        return $response->withCookie(cookie()->forever($cookies));
    }
}
