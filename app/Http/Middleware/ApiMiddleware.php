<?php

namespace App\Http\Middleware;

use Closure;

use App\Models\App;
use App\Models\Person;
use App\Models\Order;

class ApiMiddleware
{

    protected function getApp($apiKey)
    {
        return App::with(['keys'])
            ->where('api_keys.key', $apiKey)
            ->first();
    }

    protected function getCustomer($hash)
    {
        return Person::with(['tracking'])
            ->where('user_tracking.user_identifier', $hash)
            ->first();
    }

    protected function getOrder($hash)
    {
        return Order::with(['tracking'])
            ->where('user_tracking.user_identifier', $hash)
            ->first();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $apiKey = $request->get('key');
        $request['app'] = $this->getApp($apiKey);
        $hash = $request->get('person');
        $request['customer'] = $this->getCustomer($hash);
        return $next($request);
    }
}
