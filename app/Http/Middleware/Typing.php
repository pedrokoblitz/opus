<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;

use App\Repositories\AppRepository;
use App\Repositories\TypeRepository;

class Typing
{
    private $typeRepository;

    public function __construct(TypeRepository $typeRepo, AppRepository $appRepo)
    {
        $this->typeRepository = $typeRepo;
        $this->appRepository = $appepo;
    }

    public function handle($request, Closure $next)
    {
        $app = $this->appRepository->find;
        $app_id = Session::get('app_id') ? Session::get('app_id') : App::where('default', 1)->first()->id;
        $filters = [
            'slug' => $request->route('type'),
            'app_id' => $app_id
        ];
        $type = $this->typeRepository
            ->with('properties', 'statuses', 'allowedChildren')
            ->findWhere($filters)
            ->first();

        $request['app'] = $app;
        $request['type'] = $type;
        
        $response = $next($request);
        return $response;
    }
}
