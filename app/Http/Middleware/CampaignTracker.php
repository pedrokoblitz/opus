<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\Person;
use App\Models\Campaign;
use Cookie;
use Log;

class CampaignTracker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $campaignCode = $request->get('utm_campaign');
        $campaign = Campaign::where('code', $campaignCode);
        if ($campaign) {

        }

        return $next($request);
    }
}
