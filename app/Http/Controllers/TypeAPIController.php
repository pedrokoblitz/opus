<?php

namespace App\Http\Controllers;

use App\Http\Controllers\BaseController;
use Response;
use Request;
use Session;
use App\Http\Requests\CreateTypeAPIRequest;
use App\Http\Requests\UpdateTypeAPIRequest;

class TypeAPIController extends BaseController
{
    private $service;

    public function __construct(\App\Services\TypeService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $types = $this->service->all();
        return $this->sendResponse($types->toArray(), 'Types retrieved successfully');
    }

    public function store(CreateTypeAPIRequest $request)
    {
        $input = $request->all();
        $type = $this->service->create($input['type']);

        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }
        return $this->sendResponse($type->toArray(), $message);
    }

    public function show($id, Request $request)
    {
        $type = $this->service->findWithoutFail($id);

        if (empty($type)) {
            return $this->sendError('Type not found');
        }

        return $this->sendResponse($type->toArray(), 'Type retrieved successfully');
    }

    public function showBySlug($slug, Request $request)
    {
        $type = $this->service->findBySlugWithoutFail($slug);

        if (empty($type)) {
            return $this->sendError('Type not found');
        }

        return $this->sendResponse($type->toArray(), 'Type retrieved successfully');
    }

    public function update($id, UpdateTypeAPIRequest $request)
    {
        $type = $this->service->findWithoutFail($id);

        if (empty($type)) {
            return $this->sendError('Type not found');
        }

        $input = $request->all();
        $type = $this->service->update($input['type'], $id);

        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }

        return $this->sendResponse($type->toArray(), $message);
    }

    public function destroy($id, Request $request)
    {
        $this->service->delete($id);
        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }
        return $this->sendResponse($id, $message);
    }
}
