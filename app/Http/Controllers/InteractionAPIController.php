<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCommentAPIRequest;
use App\Http\Requests\UpdateCommentAPIRequest;
use App\Http\Requests\SyncTagsAPIRequest;
use App\Models\Interaction;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use Response;
use Auth;

class InteractionAPIController extends BaseController
{
    private $service;

    public function __construct(\App\Services\InteractionService $service)
    {
        $this->service = $service;
    }

    public function tag($type, $id, Request $request)
    {
        $user = Auth::user();
        $input = $request->all();
        $input['node_id'] = $id;
        $input['user_id'] = Auth::id();
        $node = $this->service->tag($input);
        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }
        return $this->sendResponse($node->toArray(), $message);

    }

    public function untag($type, $id)
    {
        $user = Auth::user();
        $input['node_id'] = $id;
        $input['user_id'] = $user->id;
        $id = $this->service->untag($input);
        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }
        return $this->sendResponse($id, $message);
    }

    public function like($type, $id)
    {
        $user = Auth::user();
        $item = [
            'user_id' => $user->id,
            'node_id' => $id
        ];
        $res = $this->service->like($item);
        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }
        return $this->sendResponse($res->toArray(), "Liked");
    }

    public function unlike($type, $id)
    {
        $user = Auth::user();
        $item = [
            'user_id' => $user->id,
            'node_id' => $id
        ];
        $res = $this->service->unlike($item);
        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }
        return $this->sendResponse($res, "Unliked");
    }

    public function vote($type, $id, Request $request)
    {
        $user = Auth::user();

        $input = $request->all();
        $item = [
            'user_id' => $user->id,
            'node_id' => $id,
            'vote' => $input['vote']
        ];

        $res = $this->service->vote($item);
        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }
        return $this->sendResponse($res->toArray(), "Voted");
    }

    public function unvote($type, $id)
    {
        $user = Auth::user();

        $item = [
            'user_id' => $user->id,
            'node_id' => $id
        ];

        $res = $this->service->unvote($item);
        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }
        return $this->sendResponse($res, "Unvoted");
    }

    public function comment($type, $id, Request $request)
    {
        $user = Auth::user();

        $input = $request->all();
        $item = [
            'user_id' => $user->id,
            'node_id' => $id,
            'comment' => $input['comment']
        ];

        $res = $this->service->comment($item);
        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }
        return $this->sendResponse($res->toArray(), "Commented");
    }

    public function uncomment($type, $id, $comment_id)
    {
        $user = Auth::user();

        $item = [
            'user_id' => $user->id,
            'id' => $comment_id
        ];
        $res = $this->service->uncomment($item);
        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }
        return $this->sendResponse($res, "Uncommented");
    }

    public function graph($type, $id, Request $request)
    {
        $user = Auth::user();

        $input = $request->all();
        $this->service->graph($input, $id);

        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }
        return $this->sendResponse($node->toArray(), $message);
    }

    public function ungraph($type, $id, $target_id)
    {
        $user = Auth::user();

        $input = $request->all();
        $this->service->ungraph($input, $id);

        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }
        return $this->sendResponse($node, $message);
    }
}
