<?php

namespace App\Http\Controllers\Application;

use App\Http\Controllers\BaseController;

use App\Models\Person;
use App\Models\Lead;

use Illuminate\Http\Request;
use Flash;
use Response;

class JourneyController extends BaseController
{
	public function capture(Request $request)
	{
		$this->getApp();
		$newLead = false;

		$only = ['name', 'phone', 'email'];
		$personData = $request->only($only);
		$person = Person::where('email', $personData['email']);
		if (!$person) {
			$newLead = true;
			$person = new Person();
		}
		$person->fill($personData);
		$person->save();
		
		$only = ['event', 'message'];
		$leadData = $request->only($only);
		$leadData['person_id'] = $person->id;
		$leadData['app_id'] = $this->app->id;

		$lead = new Lead();
		$lead->fill($leadData);
		$lead->save();

		$responseData = [];

		return $this->sendResponse($responseData, "Informações enviadas com sucesso");
	}
}
