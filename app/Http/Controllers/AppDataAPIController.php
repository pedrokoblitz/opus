<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;

use Auth;
use Session;

use Carbon\Carbon;

use App\Models\App;
use App\Models\Role;
use App\Models\User;

use App\Models\Taxonomy;
use App\Models\Graph;
use App\Models\Interaction;

use App\Models\Setting;
use App\Models\Node;
use App\Models\Term;
use App\Models\Type;
use App\Models\NodeProperty;

class AppDataAPIController extends BaseController
{
    public function __construct()
    {
    }

    public function activity()
    {
        $since = Carbon::today();
        $interactions = Interaction::with(['user', 'node', 'type'])->where([['created_at', '>', $since]])->get();
        $graph = Graph::with(['user', 'source', 'target', 'relationship'])->where([['created_at', '>', $since]])->get();
        $taxonomy = Taxonomy::with(['user', 'node', 'node.type', 'term'])->where([['created_at', '>', $since]])->get();

        $data = [
            'taxonomy' => $taxonomy->toArray(),
            'interactions' => $interactions->toArray(),
            'graph' => $graph->toArray(),
        ];

        return $this->sendResponse($data, "");
    }

    public function admin()
    {
        try {
            $apps = App::all();
            $with = [
                'allowedParents',
                'allowedChildren',
                'statuses',
                'properties',
                'sources',
                'targets'
            ];
            $types = Type::with($with)->get();
            $config = Setting::all();
            $users = User::with(['profiles']);
            $roles = Role::with(['permissions'])->get();
        } catch (\Exception $e) {
            if (app()->environment() === 'local') {
                throw $e;
            } else {
                \Log::info($e->getMessage());
                return $this->sendError($e->getMessage());
            }
        }
        $data = [
            'users' => $users,
            'apps' => $apps,
            'roles' => $roles,
            'config' => $config,
            'types' => $types
        ];
        return $this->sendResponse($data, "success!");
    }
    public function me()
    {
        try {
            $user = Auth::user();
            $user_config = Setting::where('user_id', $user->id)->get();
        } catch (\Exception $e) {
            if (app()->environment() === 'local') {
                throw $e;
            } else {
                \Log::info($e->getMessage());
                return $this->sendError($e->getMessage());
            }
        }
        $data = [
            'user_config' => $user_config,
            'user' => [
                'id' => $user->id,
                'email' => $user->email,
                'profiles' => $user->profiles
            ],
        ];
        return $this->sendResponse($data, 'App data loaded successfully');
    }

    public function app()
    {
        try {
            $app_id = Session::get('app_id') ? Session::get('app_id') : App::where('default', 1)->first()->id;
            $app = App::find($app_id);
            $config = Setting::where('user_id', 0)->get();
            $properties = NodeProperty::all();
            $terms = Term::all();
            $types = Type::with(['allowedParents', 'allowedChildren', 'statuses', 'properties', 'sources', 'targets'])->where('app_id', $app_id)->get();
            $rootNodes = Node::with(['type', 'type.properties', 'status'])->where('parent_id', 0)->get();
            // $rootTypes = Type::join('allowed_type_hierarchy', 'allowed_type_hierarchy.child_id', '=', 'types.id')->where('allowed_type_hierarchy.parent_id', 0)->get();
        } catch (\Exception $e) {
            if (app()->environment() === 'local') {
                throw $e;
            } else {
                \Log::info($e->getMessage());
                return $this->sendError($e->getMessage());
            }
        }

        $data = [
            'app' => $app,
            'config' => $config,
            'properties' => $properties,
            'terms' => $terms,
            'types' => $types,
            'nodes' => $rootNodes,
        ];
        return $this->sendResponse($data, 'App data loaded successfully');
    }
}
