<?php

namespace App\Http\Controllers;

use App\Http\Requests\RegisterAPIRequest;
use App\Http\Requests\AuthorizeAPIRequest;
use App\Models\User;
use App\Repositories\ApiKeyRepository;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Response;

class AuthAPIController extends BaseController
{
    public function register(Request $request)
    {
        $data = $request->all();
        return $this->sendResponse($data, 'registered!');
    }

    public function auth(Request $request)
    {
        $data = $request->all();
        \Auth::loginUsingId(1);
        return $this->sendResponse($data, 'authenticated!');
    }
}
