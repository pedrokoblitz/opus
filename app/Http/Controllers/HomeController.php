<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sql = "SELECT COUNT(t1.id) AS count, date FROM schedule t1
            JOIN units t2 ON t1.unit_id=t2.id
            WHERE t2.condo_id=1
            GROUP BY `date`";
    
        $data = DB::select(DB::raw($sql));
        $schedule = [];
        $schedule['labels'] = array_map(function ($i) {
            return Carbon::createFromFormat('Y-m-d', $i->date)->format('d/m');
        }, $data);
        $schedule['data'] = array_map(function ($i) {
            return $i->count;
        }, $data);

        $sql = "SELECT COUNT(t1.id) AS count, t2.name AS name FROM schedule t1
            JOIN units t2 ON t1.unit_id=t2.id
            WHERE t2.condo_id=1
            GROUP BY `name`";
        $condos = DB::select(DB::raw($sql));

        $sql = "SELECT COUNT(t1.id) AS count, t2.name AS name FROM schedule t1
            JOIN services t2 ON t1.service_id=t2.id
            JOIN units t3 ON t1.unit_id=t3.id
            WHERE t3.condo_id=1
            GROUP BY `name`";
        $services = DB::select(DB::raw($sql));

        $sql = "SELECT COUNT(t1.id) AS count, t2.name AS name FROM schedule t1
            JOIN timeframes t2 ON t1.timeframe_id=t2.id
            JOIN units t3 ON t1.unit_id=t3.id
            WHERE t3.condo_id=1
            GROUP BY `name`";
        $timeframes = DB::select(DB::raw($sql));

        $colors = [
            '#f56954',
            '#d34732',
            '#b12510',
        ];

        return view('home')
            ->with('colors', $colors)
            ->with('schedule', $schedule)
            ->with('timeframes', $timeframes)
            ->with('condos', $condos)
            ->with('services', $services);
    }
}
