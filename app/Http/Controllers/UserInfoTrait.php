<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Role;

use Auth;
use Session;

trait UserInfoTrait
{
	protected function getCurrentUser()
	{
		$with = [];
		return User::with($with)->find(Auth::id());
	}

	protected function getCurrentRole()
	{
		$with = [];
		$role_id = Session::get('current_role_id');
		return Role::with($with)->find($role_id);
	}
}
