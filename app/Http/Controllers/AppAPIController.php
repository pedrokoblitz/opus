<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateAppAPIRequest;
use App\Http\Requests\UpdateAppAPIRequest;
use App\Models\App;
use App\Repositories\AppRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Response;

use App\Criteria\AppCriteria;
use App\Criteria\PermissionCriteria;

class AppAPIController extends BaseController
{
    private $appRepository;

    public function __construct(AppRepository $appRepo)
    {
        $this->appRepository = $appRepo;
    }

    public function index(Request $request)
    {
        $apps = $this->appRepository->all();
        $payload = $apps->toArray();
        return $this->sendResponse($payload, 'Apps retrieved successfully');
    }

    public function store(CreateAppAPIRequest $request)
    {
        $input = $request->all();
        $app = $this->appRepository->create($input);
        return $this->sendResponse($app->toArray(), 'App saved successfully');
    }

    public function show($id)
    {
        $app = $this->appRepository->findWithoutFail($id);
        if (empty($app)) {
            return $this->sendError('App not found');
        }
        $payload = $app->toArray();
        return $this->sendResponse($payload, 'App retrieved successfully');
    }

    public function update($id, UpdateAppAPIRequest $request)
    {
        $input = $request->all();
        $app = $this->appRepository->findWithoutFail($id);
        if (empty($app)) {
            return $this->sendError('App not found');
        }
        $app = $this->appRepository->update($input, $id);
        $payload = $app->toArray();
        return $this->sendResponse($payload, 'App updated successfully');
    }

    public function destroy($id)
    {
        $app = $this->appRepository->findWithoutFail($id);
        if (empty($app)) {
            return $this->sendError('App not found');
        }
        $app->delete();
        return $this->sendResponse($id, 'App deleted successfully');
    }
}
