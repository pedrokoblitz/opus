<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\BaseController;
use App\Http\Controllers\BaseController;
use Response;

use App\Http\Requests\CreateRoleAPIRequest;
use App\Http\Requests\UpdateRoleAPIRequest;

class RoleAPIController extends BaseController
{
    private $service;

    public function __construct(\App\Services\RoleService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $roles = $this->service->all();
        return $this->sendResponse($roles->toArray(), 'Roles retrieved successfully');
    }

    public function store(CreateRoleAPIRequest $request)
    {
        $input = $request->all();
        $role = $this->service->create($input['role']);
        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }
        return $this->sendResponse($role->toArray(), $message);
    }

    public function show($id, Request $request)
    {
        $role = $this->service->findWithoutFail($id);

        if (empty($role)) {
            return $this->sendError('Role not found');
        }

        return $this->sendResponse($role->toArray(), 'Role retrieved successfully');
    }

    public function update($id, UpdateRoleAPIRequest $request)
    {
        $role = $this->service->findWithoutFail($id);

        if (empty($role)) {
            return $this->sendError('Role not found');
        }

        $input = $request->all();
        $role = $this->service->update($input['role'], $id);

        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }
        return $this->sendResponse($role->toArray(), $message);
    }

    public function destroy($id, Request $request)
    {
        $role = $this->service->findWithoutFail($id);

        if (empty($role)) {
            return $this->sendError('Role not found');
        }

        $this->service->delete($id);
        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }
        return $this->sendResponse($id, $message);
    }
}
