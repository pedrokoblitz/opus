<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;

use App\Http\Controllers\BaseController;
use App\Http\Requests\CreateUserAPIRequest;
use App\Http\Requests\UpdateUserAPIRequest;

class UserAPIController extends BaseController
{
    private $service;

    public function __construct(\App\Services\UserService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $users = $this->service->all();

        return $this->sendResponse($users->toArray(), 'Users retrieved successfully.');
    }

    public function show($id, Request $request)
    {
        $user = $this->service->findWithoutFail($id);

        return $this->sendResponse($user->toArray(), 'User retrieved successfully.');
    }

    public function store(CreateUserAPIRequest $request)
    {
        $input = $request->all();

        $user = $this->service->create($input);

        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }
        return $this->sendResponse($user, 'User created successfully.');
    }

    public function destroy($id, Request $request)
    {
        $this->service->delete($id);

        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }
        return $this->sendResponse($id, 'User removed successfully.');
    }

    public function update($id, UpdateUserAPIRequest $request)
    {
        $user = $this->service->findWithoutFail($id);

        if (empty($user)) {
            return $this->sendError('User not found');
        }

        $input = $request->all();
        $this->service->update($input, $id);

        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }
        return $this->sendResponse([], 'User updated successfully.');
    }
}
