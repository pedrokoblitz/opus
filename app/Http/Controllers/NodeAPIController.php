<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;

use App\Http\Requests\CreateNodeAPIRequest;
use App\Http\Requests\UpdateNodeAPIRequest;

use App\Criteria\AdminNodeListCriteria;
use App\Criteria\AdminSingleNodeCriteria;
use App\Criteria\PermissionCriteria;
use App\Repositories\PermissionRepository;

use App\Http\Controllers\BaseController;
use Auth;

class NodeAPIController extends BaseController
{
    private $service;
    private $permissionRepository;

    public function __construct(\App\Services\NodeService $service, PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
        $this->service = $service;
    }

    protected function can($user, $request, $action)
    {
        $roles = $user->roles->map(function ($i) {
            return $i->slug;
        });
        if (in_array('super_admin', $roles->toArray())) {
            return true;
        }
        $this->permissionRepository->pushCriteria(new PermissionCriteria($user, $request, $action));
        $permissions = $this->permissionRepository->all();
        if ($permissions->count() === 0) {
            return false;
        }
        return true;
    }

    public function index($type, Request $request)
    {
        $user = Auth::user();
        if ($this->can($user, $request, 'READ')) {
            return $this->sendError('Unauthorized', 403);
        }

        $this->service->pushCriteria(new AdminNodeListCriteria($user, $request));
        $nodes = $this->service->all();
        return $this->sendResponse($nodes->toArray(), 'Nodes retrieved successfully');
    }

    public function store($type, CreateNodeAPIRequest $request)
    {
        $user = Auth::user();
        if ($this->can($user, $request, 'CREATE')) {
            return $this->sendError('Unauthorized', 403);
        }

        $input = $request->all();
        $node = $this->service->create($input);

        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }
        return $this->sendResponse($node->toArray(), $message);
    }

    public function show($type, $id, Request $request)
    {
        $user = Auth::user();
        if ($this->can($user, $request, 'READ')) {
            return $this->sendError('Unauthorized', 403);
        }

        $this->service->pushCriteria(new AdminSingleNodeCriteria($user, $request));
        $node = $this->service->findWithoutFail($id);

        if (empty($node)) {
            return $this->sendError('Node not found');
        }

        $message = $this->service->getMessage();
        return $this->sendResponse($node->toArray(), $message);
    }

    public function update($type, $id, UpdateNodeAPIRequest $request)
    {
        $user = Auth::user();
        if ($this->can($user, $request, 'UPDATE')) {
            return $this->sendError('Unauthorized', 403);
        }

        $node = $this->service->findWithoutFail($id);

        if (empty($node)) {
            return $this->sendError('Node not found');
        }

        $input = $request->all();
        $input['old'] = $node;
        $node = $this->service->update($input, $id);

        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }

        return $this->sendResponse($node->toArray(), $message);
    }

    public function destroy($type, $id, Request $request)
    {
        $user = Auth::user();
        if ($this->can($user, $request, 'DELETE')) {
            return $this->sendError('Unauthorized', 403);
        }

        $this->service->remove($id);
        $message = $this->service->getMessage();
        $error = $this->service->isError();
        if ($error) {
            return $this->sendError($message);
        }

        return $this->sendResponse($id, $message);
    }
}
