<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Role;

class RoleRepository extends BaseRepository
{
    protected $fieldSearchable = [];

    public function model()
    {
        return Role::class;
    }

    public function createPermissions()
    {
        
    }

    public function removePermission()
    {
        
    }

}
