<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Graph;

class GraphRepository extends BaseRepository
{
    protected $fieldSearchable = [];

    public function model()
    {
        return Graph::class;
    }

    public function createRelationshipType($input)
    {
        
    }

    public function removeRelationshipType($id)
    {
        
    }
}
