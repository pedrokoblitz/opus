<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Interaction;

class InteractionRepository extends BaseRepository
{
    protected $fieldSearchable = [];

    public function model()
    {
        return Interaction::class;
    }

    public function createInteractionType($input)
    {
        
    }

    public function removeInteractionType($id)
    {
        
    }
}
