<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Setting;

class SettingRepository extends ScopedRepository
{
    protected $fieldSearchable = [];

    public function model()
    {
        return Setting::class;
    }
}
