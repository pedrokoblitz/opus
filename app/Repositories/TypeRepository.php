<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Type;

class TypeRepository extends BaseRepository
{
    protected $fieldSearchable = [];

    public function model()
    {
        return Type::class;
    }

    public function createProperties()
    {
        
    }

    public function createStatuses()
    {
        
    }
}
