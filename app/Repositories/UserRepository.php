<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\User;

class UserRepository extends BaseRepository
{
    protected $fieldSearchable = [];

    public function model()
    {
        return User::class;
    }
}
