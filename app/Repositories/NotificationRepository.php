<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Notification;

class NotificationRepository extends BaseRepository
{
    protected $fieldSearchable = [];

    public function model()
    {
        return Notification::class;
    }
}
