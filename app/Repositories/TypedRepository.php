<?php

namespace App\Repositories;

use Exception;

abstract class TypedRepository extends BaseRepository
{
    public function createAll(array $items, $type_id)
    {
        $results = [];
        foreach ($items as $item) {
            $item['type_id'] = $type_id;
            $result = $this->create($item);
            $results[] = $result;
        }
        return $results;
    }
}
