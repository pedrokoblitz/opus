<?php

namespace App\Repositories;

use Exception;

abstract class BaseRepository extends \Prettus\Repository\Eloquent\BaseRepository
{
    public function findWithoutFail($id, $columns = ['*'])
    {
        try {
            return $this->find($id, $columns);
        } catch (Exception $e) {
            return;
        }
    }

    public function create(array $attributes)
    {
        // Have to skip presenter to get a model not some data
        $temporarySkipPresenter = $this->skipPresenter;
        $this->skipPresenter(true);
        $model = parent::create($attributes);
        $this->skipPresenter($temporarySkipPresenter);
        $model->save();

        return $this->parserResult($model);
    }

    public function update(array $attributes, $id)
    {
        // Have to skip presenter to get a model not some data
        $temporarySkipPresenter = $this->skipPresenter;
        $this->skipPresenter(true);
        $model = parent::update($attributes, $id);
        $this->skipPresenter($temporarySkipPresenter);
        $model->save();

        return $this->parserResult($model);
    }

    public function syncRelationships($name, $ids)
    {
        $this->$$name()->sync($ids);
    }

    public function saveRelationships($name, $input)
    {
        $class = $this->relationships[$name];
        foreach ($input[$name] as $value) {
            $model = new $class($value);
            $model->save();
        }
    }

    public function removeRelationships($name, $input)
    {
        $class = $this->relationships[$name];
        foreach ($input[$name] as $value) {
            $model = new $class($value);
            $model->delete();
        }
    }

    public function createRelationships($name, $input)
    {
        $class = $this->relationships[$name];
        foreach ($input[$name] as $value) {
            $model = $class::create($value);
        }
    }
}
