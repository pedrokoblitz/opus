<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Node;

class NodeRepository extends BaseRepository
{
    protected $fieldSearchable = [];

    public function model()
    {
        return Node::class;
    }

    public function syncTags($termIds)
    {

    }

    public function saveRelationships($input)
    {

    }

    public function saveParentHistory($old, $new)
    {
        
    }

    public function saveStatusHistory($old, $new)
    {
        
    }
}
