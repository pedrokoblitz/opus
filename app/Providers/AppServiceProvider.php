<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\TypeRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(TypeRepository $typeRepository)
    {
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
