<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\UserRepository;
use App\Mail\SystemAlert;
use Mail;

class SendEmailAlert extends Command
{
    protected $userRepository;
    protected $signature = 'alert';
    protected $description = 'Sends an email alert with system report';

    public function __construct(UserRepository $userRepository)
    {
        parent::__construct();
        $this->userRepository = $userRepository;
    }

    public function handle()
    {
        $admin = '';
        Mail::to($user)->send(new SystemAlert($admin));
    }
}
