<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Service
 * @package App\Models
 * @version March 29, 2020, 3:42 pm UTC
 *
 * @property string name
 * @property integer duration
 * @property integer service_type_id
 */
class Service extends Model
{
    use SoftDeletes;

    public $table = 'services';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'duration',
        'service_type_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'duration' => 'integer',
        'service_type_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'duration' => 'required',
        'service_type_id' => 'required'
    ];


    public function type()
    {
        return $this->belongsTo('App\Models\ServiceType', 'service_type_id');       
    }    
    
}
