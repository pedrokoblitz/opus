<?php

namespace App\Models;

trait FirstOrCreateBySlug
{
	public static function firstOrCreateBySlug($slug)
	{
	    $obj = static::where('slug', $slug)->first();
	    if (!$obj) {
	    	$obj = static::create(['name' => ucfirst($slug), 'slug' => $slug]);
	    }
	    return $obj;
	}
}
