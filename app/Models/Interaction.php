<?php

namespace App\Models;

use Eloquent as Model;

class Interaction extends Model
{

    public $table = 'interactions';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    
    public $fillable = [
        'node_id',
        'user_id',
        'type_id',
        'vote',
        'comment',
        'created_at',
        'updated_at',
    ];

    protected $attributes = [
        'vote' => 0,
        'comment' => ''
    ];

    protected $casts = [
        'id' => 'integer',
        'node_id' => 'integer',
        'user_id' => 'integer',
        'type_id' => 'integer',
        'vote' => 'integer',
        'comment' => 'string'
    ];

    public static $rules = [
        'node_id' => 'integer|nullable',
        'user_id' => 'integer|nullable',
        'type_id' => 'integer|nullable',
        'vote' => 'integer|nullable',
        'comment' => 'required|string'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function node()
    {
        return $this->belongsTo('App\Models\Node');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\InteractionType');
    }
}
