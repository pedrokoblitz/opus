<?php

namespace App\Models;


/**
 * Class UserInApp
 * @package App\Models
 * @version August 3, 2020, 9:13 pm UTC
 *
 * @property integer $user_id
 * @property integer $app_id
 */
class UserInApp extends \App\Models\UserInApp
{
    public $table = '';
    
    public $timestamps = false;

    public $fillable = [
    ];

    protected $casts = [
    ];

    public static $rules = [
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function app()
    {
        return $this->belongsTo('App\Models\App');
    }
}
