<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use SoftDeletes;

    public $table = 'users';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'email',
        'password',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    protected $casts = [
        'id' => 'integer',
        'email' => 'string',
        'password' => 'string',
    ];

    public static $rules = [
        'email' => 'required|string',
        'password' => 'string|nullable',
    ];

    public function nodes()
    {
        return $this->hasMany('App\Models\Node');
    }

    public function profiles()
    {
        return $this->belongsToMany(
            'App\Models\Node',
            'user_profiles',
            'node_id',
            'user_id'
        );
    }

    public function roles()
    {
        return $this->belongsToMany(
            'App\Models\Role',
            'user_profiles',
            'role_id',
            'user_id'
        );
    }

    public function config()
    {
        return $this->hasMany('App\Models\Config');
    }
}
