<?php

namespace App\Models;

use Eloquent as Model;

class StatusHistory extends Model
{
    public $table = 'status_history';
    
    public $fillable = [
        'old_status_id',
        'new_status_id'
    ];

    protected $casts = [
        'id' => 'integer',
        'old_status_id' => 'integer',
        'new_status_id' => 'integer'
    ];

    public static $rules = [
        'old_status_id' => 'integer|nullable',
        'new_status_id' => 'integer|nullable'
    ];

    public function oldParent()
    {
        return $this->belongsTo('App\Models\Setting', 'old_status_id');
    }

    public function newParent()
    {
        return $this->belongsTo('App\Models\Setting', 'new_status_id');
    }
}
