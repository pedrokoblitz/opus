<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class TypeProperty
 * @package App\Models
 * @version June 13, 2018, 3:20 pm UTC
 *
 * @property integer type_id
 * @property integer property_id
 * @property string dummy
 */
class NodeProperty extends Model
{

    public $table = 'properties';
    
    public $timestamps = false;

    public $fillable = [
        'name',
        'slug',
        'autoload',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'autoload' => 'boolean',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'string|required',
        'slug' => 'string|nullable',
        'autoload' => 'boolean',
    ];
}
