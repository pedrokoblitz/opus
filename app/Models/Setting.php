<?php

namespace App\Models;

use Eloquent as Model;

class Setting extends Model
{
    public $table = 'settings';
    
    public $timestamps = false;

    public $fillable = [
        'user_id',
        'app_id',
        'key',
        'value',
    ];

    protected $attributes = [
        'key' => '',
        'value' => '',
    ];

    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'scope_id' => 'integer',
        'app_id' => 'integer',
        'key' => 'string',
        'value' => 'string',
        'color' => 'string',
    ];

    public static $rules = [
        'user_id' => 'integer|nullable',
        'key' => 'required|string',
        'value' => 'required|string',
    ];
}
