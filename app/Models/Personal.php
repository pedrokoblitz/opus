<?php

namespace App\Models;

use Eloquent as Model;

class Personal extends Model
{
    public $table = 'personal';
    
    public $timestamps = false;

    public $fillable = [
    	'node_id',
    	'fullname',
    	'birthdate',
    	'phone',
    	'email',
    	'cpf',
    	'bio'
    ];

    public $casts = [
    	'id' => 'integer',
    	'node_id' => 'integer',
    	'fullname' => 'string',
    	'birthdate' => 'string',
    	'phone' => 'string',
    	'email' => 'string',
    	'cpf' => 'string',
    	'bio' => 'string',
    ];

    public $rules = [
    	'node_id' => 'integer|required',
    	'fullname' => 'string|nullable',
    	'birthdate' => 'string|nullable',
    	'phone' => 'string|nullable',
    	'email' => 'string|nullable',
    	'cpf' => 'string|nullable',
    	'bio' => 'string|nullable',
    ];

}
