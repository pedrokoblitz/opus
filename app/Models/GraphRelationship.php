<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class GraphRelationship
 * @package App\Models
 * @version June 12, 2018, 7:51 pm UTC
 *
 * @property integer source_type_id
 * @property integer target_type_id
 * @property string name
 */
class GraphRelationship extends Model
{

    public $table = 'relationship_types';
    
    public $timestamps = false;

    public $fillable = [
        'source_type_id',
        'target_type_id',
        'name',
        'slug',
    ];
    
    protected $casts = [
        'id' => 'integer',
        'source_type_id' => 'integer',
        'target_type_id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
    ];

    public static $rules = [
        'source_type_id' => 'integer|required',
        'target_type_id' => 'integer|required',
        'name' => 'string|required',
        'slug' => 'string|required',
    ];
}
