<?php

namespace App\Models;

use Eloquent as Model;

class ParentHistory extends Model
{
    public $table = 'parent_history';
    
    public $fillable = [
        'old_parent_id',
        'new_parent_id'
    ];

    protected $casts = [
        'id' => 'integer',
        'old_parent_id' => 'integer',
        'new_parent_id' => 'integer'
    ];

    public static $rules = [
        'old_parent_id' => 'integer|nullable',
        'new_parent_id' => 'integer|nullable'
    ];

    public function oldParent()
    {
        return $this->belongsTo('App\Models\Node', '`old_parent_id');
    }

    public function newParent()
    {
        return $this->belongsTo('App\Models\Node', '`new_parent_id');
    }
}
