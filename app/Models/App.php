<?php

namespace App\Models;

use Eloquent as Model;

class App extends Model
{
    public $table = 'apps';
    
    public $timestamps = false;

    public $fillable = [
        'name',
        'slug',
        'default',
    ];

    protected $attributes = [
    ];

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'default' => 'integer',
    ];

    public static $rules = [
        'name' => 'string|required',
        'slug' => 'string|required',
        'default' => 'integer|nullable',
    ];

    public function types()
    {
        return $this->hasMany('App\Models\Types');
    }
}
