<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class TypeProperty
 * @package App\Models
 * @version June 13, 2018, 3:20 pm UTC
 *
 * @property integer type_id
 * @property integer property_id
 * @property string dummy
 */
class TypeProperty extends Model
{

    public $table = 'types_properties_relationships';
    
    public $timestamps = false;

    public $fillable = [
        'type_id',
        'property_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'type_id' => 'integer',
        'property_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'type_id' => 'integer|required',
        'property_id' => 'integer|required',
    ];

    public function type()
    {
        return $this->belongsTo('App\Models\Type');
    }

    public function property()
    {
        return $this->belongsTo('App\Models\NodeProperty');
    }
}
