<?php

namespace App\Models;


class Lead extends Model
{
    public $table = '';
    
    public $timestamps = false;

    public $fillable = [
    ];

    protected $casts = [
    ];

    public static $rules = [
    ];

    public function app()
    {
        return $this->belongsTo('App\Models\App');
    }

    public function person()
    {
        return $this->belongsTo('App\Models\Person');
    }
}
