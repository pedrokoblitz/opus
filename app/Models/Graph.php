<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Graph
 * @package App\Models
 * @version June 12, 2018, 3:08 pm UTC
 *
 * @property integer relationship_type_id
 * @property integer source_id
 * @property integer target_id
 */
class Graph extends Model
{
    use SoftDeletes;
    
    public $table = 'graph';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];
    
    public $fillable = [
        'relationship_type_id',
        'source_id',
        'target_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'relationship_type_id' => 'integer',
        'source_id' => 'integer',
        'target_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'relationship_type_id' => 'integer|required',
        'source_id' => 'integer|required',
        'target_id' => 'integer|required'
    ];

    public function relationship()
    {
        return $this->belongsTo('App\Models\GraphRelationship', 'relationship_type_id');
    }

    public function source()
    {
        return $this->belongsTo('App\Models\Node', 'source_id');
    }

    public function target()
    {
        return $this->belongsTo('App\Models\Node', 'target_id');
    }
}
