<?php

namespace App\Models;

use Eloquent as Model;
use DB;

class Type extends Model
{

    public $table = 'types';
    
    public $timestamps = false;

    public $fillable = [
        'name',
        'slug',
        'app_id',
    ];

    protected $casts = [
        'id' => 'integer',
        'app_id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
    ];

    public static $rules = [
        'app_id' => 'integer',
        'name' => 'string|required',
        'slug' => 'string|nullable',
    ];

    public function properties()
    {
        return $this->belongsToMany('App\Models\NodeProperty', 'types_properties_relationships', 'type_id', 'property_id');
    }

    public function app()
    {
        return $this->belongsTo('App\Models\App');
    }

    public function statuses()
    {
        return $this->hasMany('App\Models\Setting')->where('scope_id', 0);
    }

    public function isRoot()
    {
        return \DB::table('allowed_type_hierarchy')->where([
            'parent_id' => 0,
            'child_id' => $this->id
        ])->count() > 0;
    }

    public function allowedChildren()
    {
        return $this->belongsToMany('App\Models\Type', 'allowed_type_hierarchy', 'parent_id', 'child_id');
    }

    public function allowedParents()
    {
        return $this->belongsToMany('App\Models\Type', 'allowed_type_hierarchy', 'child_id', 'parent_id');
    }

    public function sources()
    {
        return $this->belongsToMany('App\Models\Type', 'relationship_types', 'source_type_id', 'target_type_id');
    }

    public function targets()
    {
        return $this->belongsToMany('App\Models\Type', 'relationship_types', 'target_type_id', 'source_type_id');
    }
}
