<?php

namespace App\Models;

use Eloquent as Model;

class InteractionType extends Model
{
    public $table = 'interaction_types';
    
    public $timestamps = false;

    public $fillable = [
        'name',
        'slug',
        'vote',
        'comment'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'vote' => 'boolean',
        'comment' => 'boolean',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'string|required',
        'slug' => 'string|nullable',
        'vote' => 'boolean|nullable',
        'comment' => 'boolean|nullable',
    ];
}
