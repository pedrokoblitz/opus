<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class TimeTracking
 * @package App\Models
 * @version June 12, 2018, 7:51 pm UTC
 *
 * @property integer node_id
 * @property boolean whole_day
 * @property date start_date
 * @property time start_time
 * @property date end_date
 * @property time end_time
 */
class TimeTracking extends Model
{

    public $table = 'time_tracking';
    
    public $timestamps = false;



    public $fillable = [
        'node_id',
        'start_date',
        'end_date',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'node_id' => 'integer',
        'start_date' => 'datetime',
        'end_date' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'node_id' => 'integer|nullable',
        'start_date' => 'datetime|nullable',
        'end_date' => 'datetime|nullable'
    ];

    public function node()
    {
        return $this->belongsTo('App\Models\Node');
    }
}
