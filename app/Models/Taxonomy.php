<?php

namespace App\Models;

use Eloquent as Model;

class Taxonomy extends Model
{
    public $table = 'taxonomy';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'term_id',
        'user_id',
        'node_id',
        'created_at',
        'updated_at',
    ];

    protected $attributes = [
    ];

    protected $casts = [
        'id' => 'integer',
        'term_id' => 'integer',
        'user_id' => 'integer',
        'node_id' => 'integer'
    ];

    public static $rules = [
        'term_id' => 'integer|required',
        'user_id' => 'integer|required',
        'node_id' => 'integer|required'
    ];

    public function term()
    {
        return $this->belongsTo('App\Models\Term');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function node()
    {
        return $this->belongsTo('App\Models\Node');
    }
}
