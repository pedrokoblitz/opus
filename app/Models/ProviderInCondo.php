<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProviderInCondo
 * @package App\Models
 * @version March 29, 2020, 3:42 pm UTC
 *
 * @property integer provider_id
 * @property integer condo_id
 */
class ProviderInCondo extends Model
{
    use SoftDeletes;

    public $table = 'providers_in_condos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'provider_id',
        'condo_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'provider_id' => 'integer',
        'condo_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'provider_id' => 'required',
        'condo_id' => 'required'
    ];


    public function provider()
    {
        return $this->belongsTo('App\Models\Provider');
    }    

    public function condo()
    {
        return $this->belongsTo('App\Models\Condo');       
    }    

}
