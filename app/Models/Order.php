<?php

namespace App\Models;

use Eloquent as Model;

class Order extends Model
{
    public $table = '';
    
    public $timestamps = false;

    public $fillable = [
    ];

    protected $casts = [
    ];

    public static $rules = [
    ];

    public function payment_status()
    {
        return $this->belongsTo('App\Models\Setting');
    }   
    public function shipping_status()
    {
        return $this->belongsTo('App\Models\Setting');
    }   

    public function payment_history()
    {
        return $this->hasMany('App\Models\StatusHistory');
    }   

    public function shipping_history()
    {
        return $this->hasMany('App\Models\StatusHistory');
    }   

    public function app()
    {
        return $this->belongsTo('App\Models\App');
    }   

    public function customer()
    {
        return $this->belongsTo('App\Models\Person');
    }   

    public function shipping_address()
    {
        return $this->belongsTo('App\Models\Address');
    }   

    public function coupon()
    {
        return $this->belongsTo('App\Models\Coupon');
    }   
    
    public function items()
    {
        return $this->hasMany('App\Models\OrderItem');
    }
}
