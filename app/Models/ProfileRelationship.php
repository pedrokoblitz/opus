<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class UserProfile
 * @package App\Models
 * @version June 12, 2018, 7:51 pm UTC
 *
 * @property integer user_id
 * @property integer role_id
 * @property integer node_id
 */
class ProfileRelationship extends Model
{

    public $table = 'user_profiles';
    
    public $timestamps = false;

    public $fillable = [
        'user_id',
        'role_id',
        'node_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'role_id' => 'integer',
        'node_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'user_id' => 'integer|nullable',
        'role_id' => 'integer|nullable',
        'node_id' => 'integer|nullable'
    ];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

}
