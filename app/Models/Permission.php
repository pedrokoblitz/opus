<?php

namespace App\Models;

use Eloquent as Model;

class Permission extends Model
{

    public $table = 'permissions';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'type_id',
        'role_id',
        'action',
    ];

    protected $casts = [
        'id' => 'integer',
        'type_id' => 'integer',
        'role_id' => 'integer',
        'action' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'type_id' => 'integer|required',
        'role_id' => 'integer|required',
        'action' => 'string|required',
    ];

    public function type()
    {
        return $this->belongsTo('App\Models\Type');
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }
}
