<?php

namespace App\Models;


/**
 * Class OrderStatusHistory
 * @package App\Models
 * @version June 22, 2020, 6:00 pm UTC
 *
 * @property integer $order_id
 * @property integer $old_status_id
 * @property integer $new_status_id
 */

use Eloquent as Model;

class ShippingStatusHistory extends Model
{
    public function old_status()
    {
        return $this->belongsTo('App\Models\ShippingStatus');
    }

    public function new_status()
    {
        return $this->belongsTo('App\Models\ShippingStatus');
    }
}
