<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Unit
 * @package App\Models
 * @version March 29, 2020, 3:42 pm UTC
 *
 * @property integer condo_id
 * @property string name
 * @property integer meters
 * @property integer rooms
 */
class Unit extends Model
{
    public $table = 'units';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'condo_id',
        'name',
        'meters',
        'rooms'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'condo_id' => 'integer',
        'name' => 'string',
        'meters' => 'integer',
        'rooms' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'condo_id' => 'required',
        'name' => 'required',
        'meters' => 'required',
        'rooms' => 'required'
    ];


    public function condo()
    {
        return $this->belongsTo('App\Models\Condo');       
    }    
    
}
