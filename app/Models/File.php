<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class File
 * @package App\Models
 * @version November 23, 2017, 6:36 pm UTC
 *
 * @property integer node_id
 * @property string name
 * @property string description
 * @property string filepath
 * @property string filename
 * @property string mimetype
 * @property string extension
 * @property integer width
 * @property integer height
 */
class File extends Model
{
    public $table = 'files';
    
    public $timestamps = false;

    public $fillable = [
        'node_id',
        'filepath',
        'filename',
        'mimetype',
        'extension',
        'width',
        'height'
    ];

    protected $attributes = [
        'filepath' => '',
        'filename' => '',
        'mimetype' => '',
        'extension' => '',
        'width' => 0,
        'height' => 0,
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'node_id' => 'integer',
        'filepath' => 'string',
        'filename' => 'string',
        'mimetype' => 'string',
        'width' => 'integer',
        'height' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'node_id' => 'integer|nullable',
        'filepath' => 'string|nullable',
        'filename' => 'string|nullable',
        'mimetype' => 'string|nullable',
        'width' => 'integer|nullable',
        'height' => 'integer|nullable'
    ];

    public function node()
    {
        return $this->belongsTo('App\Models\Node');
    }
}
