<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ServiceInUnit
 * @package App\Models
 * @version February 18, 2020, 6:19 pm UTC
 *
 * @property integer service_id
 * @property integer unit_id
 */
class ServiceInProvider extends Model
{
    use SoftDeletes;

    public $table = 'services_in_provider';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'service_id',
        'unit_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'service_id' => 'integer',
        'unit_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'service_id' => 'required',
        'unit_id' => 'required'
    ];


    public function service()
    {
        return $this->belongsTo('App\Models\Service');       
    }    

    public function unit()
    {
        return $this->belongsTo('App\Models\Unit');       
    }    
    
}
