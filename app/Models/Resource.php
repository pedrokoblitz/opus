<?php

namespace App\Models;

use Eloquent as Model;

class Resource extends Model
{

    public $table = 'resources';
    
    public $timestamps = false;

    public $fillable = [
        'node_id',
        'url',
        'embed',
    ];

    protected $casts = [
        'id' => 'integer',
        'node_id' => 'integer',
        'url' => 'string',
        'embed' => 'string',
    ];

    public static $rules = [
        'node_id' => 'integer|nullable',
        'url' => 'url|nullable',
        'embed' => 'string|nullable',
    ];

    public function node()
    {
        return $this->belongsTo('App\Models\Node');
    }
}
