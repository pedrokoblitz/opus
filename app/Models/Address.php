<?php

namespace App\Models;

use Eloquent as Model;

class Address extends Model
{

    public $table = 'addresses';
    
    public $timestamps = false;

    public $fillable = [
        'node_id',
        'street',
        'number',
        'address_line',
        'district',
        'city',
        'province',
        'country',
        'zipcode',
        'latitude',
        'longitude'
    ];

    protected $casts = [
        'id' => 'integer',
        'node_id' => 'integer',
        'street' => 'string',
        'number' => 'string',
        'address_line' => 'string',
        'district' => 'string',
        'city' => 'string',
        'province' => 'string',
        'country' => 'string',
        'zipcode' => 'string',
        'latitude' => 'float',
        'longitude' => 'float'
    ];

    public static $rules = [
        'node_id' => 'integer',
        'street' => 'string|nullable',
        'number' => 'string|nullable',
        'address_line' => 'string|nullable',
        'district' => 'string|nullable',
        'city' => 'string|nullable',
        'province' => 'string|nullable',
        'country' => 'string|nullable',
        'zipcode' => 'string|nullable',
        'latitude' => 'float|nullable',
        'longitude' => 'float|nullable'
    ];

    public function node()
    {
        return $this->belongsTo('App\Models\Node');
    }
}
