<?php

namespace App\Models;

use Eloquent as Model;

class Person extends Model
{
    public $table = '';
    
    public $timestamps = false;

    public $fillable = [
    ];

    protected $casts = [
    ];

    public static $rules = [
    ];

    protected $dates = ['birthdate', 'optin', 'optout', 'last_campaign'];
    
    public function steps()
    {
        return $this->hasMany('App\Models\StatusHistory');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Setting');
    }

    public function tracking()
    {
        return $this->hasMany('App\Models\Person');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order', 'customer_id');
    }
}
