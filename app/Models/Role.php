<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Role
 * @package App\Models
 * @version June 13, 2018, 3:25 pm UTC
 *
 * @property string name
 * @property string slug
 * @property string dummy
 */
class Role extends Model
{
    public $table = 'roles';
    
    public $timestamps = false;

    public $fillable = [
        'app_id',
        'name',
        'slug',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'app_id' => 'integer',
        'name' => 'string',
        'slug' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'app_id' => 'integer|required',
        'name' => 'string|required',
        'slug' => 'string|nullable',
    ];

    public function app()
    {
        return $this->belongsTo('App\Models\App');
    }

    public function permissions()
    {
        return $this->hasMany('App\Models\Permission');
    }
}
