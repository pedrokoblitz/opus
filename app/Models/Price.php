<?php

namespace App\Models;

use Eloquent as Model;

/**
 * Class Price
 * @package App\Models
 * @version June 19, 2020, 10:40 pm UTC
 *
 * @property integer $product_id
 * @property integer $price
 * @property boolean $default
 * @property integer $fixed_discount
 * @property integer $percent_discount
 * @property boolean $free_shipping
 * @property string|\Carbon\Carbon $begins
 * @property string|\Carbon\Carbon $expires
 */
class Price extends Model
{
    public $table = '';
    
    public $timestamps = false;

    public $fillable = [
    ];

    protected $casts = [
    ];

    public static $rules = [
    ];

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }  
}
