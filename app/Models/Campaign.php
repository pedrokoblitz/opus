<?php

namespace App\Models;

use Eloquent as Model;

class Campaign extends Model
{
    public $table = '';
    
    public $timestamps = false;

    public $fillable = [
    ];

    protected $casts = [
    ];

    public static $rules = [
    ];
	
    public function app()
    {
        return $this->belongsTo('App\Models\App');
    }   
}
