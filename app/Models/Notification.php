<?php

namespace App\Models;

use Eloquent as Model;

class Notification extends Model
{
    public $table = 'notifications';
    
    public $timestamps = false;

    public $fillable = [
        'user_source_id',
        'user_target_id',
        'node_source_id',
        'node_target_id',
        'action',
        'message',
    ];

    protected $casts = [
        'id' => 'integer',
        'user_source_id' => 'integer',
        'user_target_id' => 'integer',
        'node_source_id' => 'integer',
        'node_target_id' => 'integer',
        'action' => 'string',
        'message' => 'string',
    ];

    public static $rules = [
        'user_source_id' => 'required|nullable',
        'user_target_id' => 'integer|nullable',
        'node_source_id' => 'required|nullable',
        'node_target_id' => 'integer|nullable',
        'action' => 'required|string',
        'message' => 'string|nullable',
    ];
}
