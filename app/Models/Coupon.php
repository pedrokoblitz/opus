<?php

namespace App\Models;

use Eloquent as Model;

class Coupon extends Model
{
    public $table = '';
    
    public $timestamps = false;

    public $fillable = [
    ];

    protected $casts = [
    ];

    public static $rules = [
    ];

    public function app()
    {
        return $this->belongsTo('App\Models\App');
    }   

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }   

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }   
}
