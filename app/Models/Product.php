<?php

namespace App\Models;


/**
 * Class Product
 * @package App\Models
 * @version June 19, 2020, 10:40 pm UTC
 *
 * @property integer $app_id
 * @property integer $book_id
 */

use Eloquent as Model;

class Product extends Model
{
    public $table = '';
    
    public $timestamps = false;

    public $fillable = [
    ];

    protected $casts = [
    ];

    public static $rules = [
    ];

    public function app()
    {
        return $this->belongsTo('App\Models\App');
    }   

    public function prices()
    {
        return $this->hasMany('App\Models\Price');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Term', 'product_taxonomy');
    }

    public function images()
    {
        return $this->hasMany('App\Models\ProductImage');
    }
        
}
