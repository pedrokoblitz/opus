<?php

namespace App\Models;

use Eloquent as Model;

class OrderItem extends Model
{
    public $table = '';
    
    public $timestamps = false;

    public $fillable = [
    ];

    protected $casts = [
    ];

    public static $rules = [
    ];

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }   

    public function price()
    {
        return $this->belongsTo('App\Models\Price');
    }
}
