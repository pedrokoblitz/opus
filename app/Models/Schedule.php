<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Schedule
 * @package App\Models
 * @version March 29, 2020, 3:42 pm UTC
 *
 * @property string date
 * @property boolean whole_day
 * @property integer schedule_status_id
 * @property integer timeframe_id
 * @property integer customer_id
 * @property integer service_id
 * @property integer unit_id
 * @property integer provider_id
 * @property integer invoice_id
 */
class Schedule extends Model
{
    use SoftDeletes;

    public $table = 'schedule';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'date',
        'whole_day',
        'schedule_status_id',
        'timeframe_id',
        'customer_id',
        'service_id',
        'unit_id',
        'provider_id',
        'invoice_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'date' => 'date',
        'whole_day' => 'boolean',
        'schedule_status_id' => 'integer',
        'timeframe_id' => 'integer',
        'customer_id' => 'integer',
        'service_id' => 'integer',
        'unit_id' => 'integer',
        'provider_id' => 'integer',
        'invoice_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'date' => 'required',
        'whole_day' => 'required',
        'schedule_status_id' => 'required',
        'timeframe_id' => 'required',
        'customer_id' => 'required',
        'service_id' => 'required',
        'unit_id' => 'required',
        'provider_id' => 'required',
        'invoice_id' => 'required'
    ];


    public function status()
    {
        return $this->belongsTo('App\Models\ScheduleStatus', 'schedule_status_id');
    }    

    public function timeframe()
    {
        return $this->belongsTo('App\Models\Timeframe');
    }    

    public function customer()
    {
        return $this->belongsTo('App\Models\User');
    }    

    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }    

    public function unit()
    {
        return $this->belongsTo('App\Models\Unit');       
    }    

    public function provider()
    {
        return $this->belongsTo('App\Models\User');       
    }    

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoice');       
    }    

}
