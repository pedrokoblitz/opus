<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Node extends Model
{
    use SoftDeletes;

    public $table = 'nodes';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'user_id',
        'type_id',
        'parent_id',
        'status_id',
        'order',
        'level',
        'name',
        'slug',
        'visibility',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'type_id' => 'integer',
        'status' => 'integer',
        'order' => 'integer',
        'level' => 'integer',
        'name' => 'string',
        'slug' => 'string',
        'status_id' => 'integer',
    ];

    public static $rules = [
        'user_id' => 'integer|required',
        'type_id' => 'integer|required',
        'parent_id' => 'integer|required',
        'order' => 'integer|nullable',
        'level' => 'integer|nullable',
        'name' => 'string|required',
        'slug' => 'string|required',
        'status_id' => 'integer|required',
    ];

    public function parent()
    {
        return $this->belongsTo('App\Models\Node', 'parent_id', 'id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Node', 'parent_id', 'id');
    }

    public function siblings()
    {
        return $this->hasMany('App\Models\Node', 'parent_id', 'parent_id');
    }

    public function content()
    {
        return $this->hasOne('App\Models\Content');
    }

    public function type()
    {
        return $this->belongsTo('App\Models\Type');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\TypeStatus');
    }

    public function interactions()
    {
        return $this->hasMany('App\Models\Interaction');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Models\Term', 'taxonomy');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function file()
    {
        return $this->hasOne('App\Models\File');
    }

    public function resource()
    {
        return $this->hasOne('App\Models\Resource');
    }

    public function address()
    {
        return $this->hasOne('App\Models\Address');
    }

    public function event()
    {
        return $this->hasOne('App\Models\EventInformation');
    }

    public function personal()
    {
        return $this->hasOne('App\Models\Personal');
    }
}

