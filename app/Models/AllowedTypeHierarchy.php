<?php

namespace App\Models;

use Eloquent as Model;

class AllowedTypeHierarchy extends Model
{
    public $table = 'allowed_type_hierarchy';
    
    public $timestamps = false;

    public $fillable = [
        'parent_id',
        'child_id',
    ];

    protected $casts = [
        'id' => 'integer',
        'parent_id' => 'integer',
        'child_id' => 'integer',
    ];

    public static $rules = [
        'parent_id' => 'integer|required',
        'child_id' => 'integer|required',
    ];

    public function parent()
    {
        return $this->belongsTo('App\Models\Type');
    }

    public function child()
    {
        return $this->belongsTo('App\Models\Type');
    }
}
