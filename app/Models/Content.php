<?php

namespace App\Models;

use Eloquent as Model;

class Content extends Model
{
    public $table = 'content';
    
    public $timestamps = false;

    protected $attributes = [
    ];

    public $fillable = [
        'node_id',
        'slug',
        'title',
        'subtitle',
        'body'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'node_id' => 'integer',
        'language' => 'string',
        'slug' => 'string',
        'title' => 'string',
        'subtitle' => 'string',
        'body' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'node_id' => 'integer|nullable',
        'language' => 'string|nullable',
        'slug' => 'string|nullable',
        'description' => 'string|nullable',
        'title' => 'string|nullable',
        'subtitle' => 'string|nullable',
        'tagline' => 'string|nullable',
        'excerpt' => 'string|nullable',
        'body' => 'string|nullable'
    ];

    public function node()
    {
        return $this->belongsTo('App\Models\Node');
    }
}
