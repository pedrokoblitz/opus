<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Location
 * @package App\Models
 * @version March 29, 2020, 3:42 pm UTC
 *
 * @property string name
 * @property integer responsible_id
 * @property integer address_id
 * @property integer condo_type_id
 */
class Location extends Model
{
    use SoftDeletes;

    public $table = 'locations';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'name',
        'responsible_id',
        'address_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'responsible_id' => 'integer',
        'address_id' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'responsible_id' => 'required',
        'address_id' => 'required',
    ];

    public function address()
    {
        return $this->belongsTo('App\Models\Address');       
    }

    public function responsible()
    {
        return $this->belongsTo('App\Models\User');       
    }

    public function units()
    {
        return $this->hasMany('App\Models\Unit');
    }
}
