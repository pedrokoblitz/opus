<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Invoice
 * @package App\Models
 * @version March 29, 2020, 3:42 pm UTC
 *
 * @property integer customer_id
 * @property integer invoice_status_id
 * @property string|\Carbon\Carbon expires
 */
class Invoice extends Model
{
    use SoftDeletes;

    public $table = 'invoices';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'customer_id',
        'status_id',
        'value',
        'expires'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'customer_id' => 'integer',
        'status_id' => 'integer',
        'value' => 'integer',
        'expires' => 'datetime'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'customer_id' => 'required',
        'invoice_status_id' => 'required',
        'expires' => 'required'
    ];

    public function customer()
    {
        return $this->belongsTo('App\Models\User');       
    }    

    public function address()
    {
        return $this->belongsTo('App\Models\Address');       
    }    

    public function status()
    {
        return $this->belongsTo('App\Models\Setting', 'status_id');
    }    
}
