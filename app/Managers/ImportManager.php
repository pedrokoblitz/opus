<?php

namespace App\Managers;

use Services\TypeService;
use Services\NodeService;
use Services\InteractionService;

class ImportManager
{
    public function __construct()
    {

    }

    public function importAppDefinition()
    {

    }

    public function importNode()
    {

    }

    public function importNodeTree()
    {

    }
}