<?php

namespace App\Services;

use Str;

trait SluggableTrait {

    public function generateSlug($name, $repository)
    {
        $slug = Str::slug($name, '-');
        $items = $repository->findWhere([['slug', 'like', $slug . "%"]])->count();
        if ($items === 0) {
            return $slug;
        }
        return $slug . '-' . $items;
    }
}
