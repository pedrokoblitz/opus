<?php

namespace App\Services;

use DB;
use App\Repositories\UserRepository;
use App\Repositories\RoleRepository;
use App\Repositories\NodeRepository;
use App\Repositories\TypeRepository;
use App\Repositories\PersonalRepository;
use App\Repositories\ProfileRelationshipRepository;

class UserService
{
    use ErrorHandlingTrait, SluggableTrait;

    public function __construct(
        TypeRepository $typeRepo,
        UserRepository $userRepo,
        RoleRepository $roleRepo,
        NodeRepository $nodeRepo,
        PersonalRepository $personalRepo,
        ProfileRelationshipRepository $profileRelationshipRepo
    ) {
        $this->typeRepository = $typeRepo;
        $this->userRepository = $userRepo;
        $this->roleRepository = $roleRepo;
        $this->nodeRepository = $nodeRepo;
        $this->personalRepository = $personalRepo;
        $this->profileRelationshipRepository = $profileRelationshipRepo;
    }

    public function all()
    {
        return $this->userRepository->all();
    }

    public function findWithoutFail($id)
    {
        return $this->userRepository->findWithoutFail($id);
    }

    public function create($input)
    {
        $role = $input['user']['role'];
        $personal = $input['user']['personal'];
        unset($input['user']['role']);
        unset($input['user']['personal']);
        $password = bcrypt($input['user']['password']);
        $input['user']['password'] = $password;

        $profileType = $this->typeRepository->findWhere(['slug' => 'profile'])->first();
        if (!$profileType) {
            $profileType = $this->typeRepository->create([
                'name' => 'Profile',
                'slug' => 'profile'
            ]);
        }

        $node = [];
        $node['type_id'] = $profileType->id;
        $node['name'] = $personal['fullname'];
        $node['slug'] = $this->generateSlug($node['name'], $this->nodeRepository);

        DB::beginTransaction();

        try {
            $user = $this->userRepository->create($input['user']);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->handleError($e);            
            return;
        }

        try {
            $node['user_id'] = $user->id;
            $node = $this->nodeRepository->create($node);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->handleError($e);            
            return;
        }

        $personal['node_id'] = $node->id;
        try {
            $personal = $this->personalRepository->create($personal);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->handleError($e);            
            return;
        }

        $roleProfile = [
            'role_id' => $role,
            'user_id' => $user->id,
            'node_id' => $personal->id             
        ];

        try {
            $this->profileRelationshipRepository->create($roleProfile);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->handleError($e);            
            return;
        }

        DB::commit();
        return $user;
    }

    public function update($input, $id)
    {
        if (isset($input['user']['password'])) {
            $input['user']['password'] = bcrypt($input['user']['password']);
        }

        DB::beginTransaction();
        try {
            $user = $this->userRepository->update($input['user'], $id);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->handleError($e);            
            return;
        }

        $personal_id = $input['node']['personal']['id'];
        unset($input['node']['personal']['id']);

        $personal = $input['node']['personal'];
        unset($input['node']['personal']);

        $node = $input['node'];
        $node_id = $input['node']['id'];
        unset($input['node']['id']);

        try {
            $personal = $this->personalRepository->update($personal, $personal_id);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->handleError($e);            
            return;
        }

        try {
            $node = $this->nodeRepository->update($node, $node_id);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->handleError($e);            
            return;
        }

        DB::commit();
        return $user;
    }
}
