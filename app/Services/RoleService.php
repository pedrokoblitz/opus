<?php

namespace App\Services;

use DB;

use App\Repositories\RoleRepository;
use App\Repositories\PermissionRepository;

class RoleService
{
    use ErrorHandlingTrait, SluggableTrait;

    private $roleRepository;
    private $permissionRepository;

    public function __construct(
        RoleRepository $roleRepository,
        PermissionRepository $permissionRepository
    ) {
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
    }

    public function all()
    {
        return $this->roleRepository->with(['permissions'])->all();
    }

    public function findWithoutFail($id)
    {
        return $this->roleRepository->findWithoutFail($id);
    }

    public function create($input)
    {
        DB::beginTransaction();

        $input['slug'] = $this->generateSlug($input['name'], $this->roleRepository);

        try {
            $role = $this->roleRepository->create($input);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->handleError($e);            
            return;
        }

        $permissionIds = [];
        foreach ($input['permissions'] as $permission) {
            try {
                $permissionHasId = isset($permission['id']) && $permission['id'] > 0;
                if (!$permissionHasId) {
                    $permission['role_id'] = $role->id;
                    $permission['id'] = $this->permissionRepository->create($permission)->id;
                }
            } catch (\Exception $e) {
                DB::rollBack();
                $this->handleError($e);            
                return;
            }
        }

        DB::commit();
        return $role;
    }

    public function update($input, $id)
    {
        $role = $this->roleRepository->findWithoutFail($id);

        if (empty($role)) {
            $this->error = true;
            $this->messages[] = "Role not found.";
            return;
        }

        if (!isset($input['name']) || !$input['name']) {
            $input['name'] = $role->name;
        }

        if ($role->name !== $input['name']) {
            $input['slug'] = $this->generateSlug($input['name'], $this->roleRepository);
        }

        DB::beginTransaction();

        $permissions = $input['permissions'];
        unset($input['app_id']);
        unset($input['permissions']);

        try {
            $role = $this->roleRepository->update($input, $id);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->handleError($e);            
            return;
        }

        $oldPermissionsIds = $role->permissions->map(function ($i) {
            return $i->id;
        })->toArray();

        $newPermissionsIds = collect($permissions)->filter(function ($i) {
            return isset($i->id) && $i->id > 0;
        })->map(function ($i) {
            return $i->id;
        })->toArray();

        foreach ($oldPermissionsIds as $oldPermissionId) {
            if (!in_array($oldPermissionId, $newPermissionsIds)) {
                try {
                    $this->permissionRepository->delete($oldPermissionId);
                } catch (\Exception $e) {
                    DB::rollBack();
                    $this->handleError($e);            
                    return;
                }
            }
        }

        foreach ($permissions as $permission) {
            try {
                $permissionHasId = isset($permission['id']) && $permission['id'] > 0;
                if (!$permissionHasId) {
                    $permission['role_id'] = $role->id;
                    $permission['id'] = $this->permissionRepository->create($permission)->id;
                }
                if ($permissionHasId) {
                    $this->permissionRepository->update($permission, $permission['id']);
                }
            } catch (\Exception $e) {
                DB::rollBack();
                $this->handleError($e);            
                return;
            }
        }

        DB::commit();
        return $role;
    }

    public function delete($id)
    {
        $role = $this->roleRepository->findWithoutFail($id);
        if (!$role) {
            $this->error = true;
            $this->messages[] = "Role not found.";
            return;
        }
        if ($role->users->count()) {
            $this->error = true;
            $this->messages[] = "Role still has users.";
            return;
        }
        return $this->roleRepository->delete($id);
    }
}
