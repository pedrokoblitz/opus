<?php

namespace App\Services;

use Session;

abstract class AbstractService
{    
    protected $typeRepository;
    protected $error = false;
    protected $messages = [];

    public function __construct(
        TypeRepository $typeRepo,
    ) {
        $this->typeRepository  = $typeRepo;
        $this->app = $this->getCurrentApp();
    }
    
    protected function handleError($e)
    {
        if (app()->environment() === 'local') {
            throw $e;
        } else {
	        \Log::info($e->getMessage());
	        $this->messages[] = $e->getMessage();
	        $this->error = true;
        }
    }

    public function isError()
    {
        return $this->error;
    }

    public function getMessage()
    {
        return $this->messages;
    }

    protected function getCurrentApp()
	{
        $app_id = Session::get('app_id'); 
        if ($app_id) {
            return App::where('id', $app_id)->first();
        }
        if (!$app_id) {
            return App::where('default', 1)->first();
        }
    }
}
