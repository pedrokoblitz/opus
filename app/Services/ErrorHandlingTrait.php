<?php

namespace App\Services;

trait ErrorHandlingTrait
{
    protected $error = false;
    protected $messages = [];
    
    protected function handleError($e)
    {
        if (app()->environment() === 'local') {
            throw $e;
        } else {
	        \Log::info($e->getMessage());
	        $this->messages[] = $e->getMessage();
	        $this->error = true;
        }
    }

    public function isError()
    {
        return $this->error;
    }

    public function getMessage()
    {
        return $this->messages;
    }
}
