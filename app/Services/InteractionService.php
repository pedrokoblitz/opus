<?php

namespace App\Services;

use App\Repositories\NodeRepository;
use App\Repositories\InteractionRepository;
use App\Repositories\InteractionTypeRepository;
use App\Repositories\GraphRepository;
use App\Repositories\TermRepository;
use App\Repositories\TaxonomyRepository;

class InteractionService
{
	use ErrorHandlingTrait;
	
	public function __construct(
		NodeRepository $nodeRepository,
		InteractionRepository $interactionRepository,
		InteractionTypeRepository $interactionTypeRepository,
		GraphRepository $graphRepository,
		TermRepository $termRepository,
		TaxonomyRepository $taxonomyRepository
	) {
		$this->nodeRepository = $nodeRepository;
		$this->interactionRepository = $interactionRepository;
		$this->interactionTypeRepository = $interactionTypeRepository;
		$this->graphRepository = $graphRepository;
		$this->termRepository = $termRepository;
		$this->taxonomyRepository = $taxonomyRepository;
	}

	public function like($input)
	{
		$interactionType = $this->interactionRepository->findWhere(['slug' => 'like']);
		if (!$interactionType) {
			$interactionType = $this->interactionRepository->create([
				'name' => 'Like',
				'slug' => 'like',
			]);
		}
		$input['interaction_type_id'] = $interactionType->id;

		$node = $this->nodeRepository->findWithoutFail($input['node_id']);
		if ($node && !$node->like) {
            $this->messages[] = 'You can not like this node.';
            $this->error = true;
            return;
		}
		$liked = $this->interactionTypeRepository->findWhere($input)->first();
		if ($liked) {
            $this->messages[] = 'Already liked.';
            $this->error = true;
            return;
		}
		try {
			return $this->interactionTypeRepository->create($input);
		} catch (\Exception $e) {
			$this->handleError($e);		
            return;
		}
	}
	
	public function unlike($input)
	{
		$liked = $this->interactionTypeRepository->findWhere($input)->first();
		if ($liked) {
			return $this->interactionTypeRepository->deleteWhere($input);
		}
        $this->messages[] = 'Not liked.';
        $this->error = true;
        return;
	}
	
	public function vote($input)
	{
		$interactionType = $this->interactionRepository->findWhere(['slug' => 'vote']);
		if (!$interactionType) {
			$interactionType = $this->interactionRepository->create([
				'name' => 'Vote',
				'slug' => 'vote',
				'vote' => 1
			]);
		}
		$input['interaction_type_id'] = $interactionType->id;

		$node = $this->nodeRepository->findWithoutFail($input['node_id']);
		if (!$node) {
            $this->messages[] = 'Node not found.';
            $this->error = true;
            return;
		}
		if ($node && !$node->vote) {
            $this->messages[] = 'You can not vote this node.';
            $this->error = true;
            return;
		}
		$where = ['user_id' => $input['user_id'], 'node_id' => $input['node_id']];
		$voted = $this->interactionTypeRepository->findWhere($where)->first();
		if ($voted) {
            $this->messages[] = 'Already voted.';
            $this->error = true;
            return;
		}
		try {
			return $this->interactionTypeRepository->create($input);
		} catch (\Exception $e) {
			$this->handleError($e);
			return;		
		}
	}
	
	public function unvote($input)
	{
		$where = ['user_id' => $input['user_id'], 'node_id' => $input['node_id']];
		$voted = $this->interactionTypeRepository->findWhere($where)->first();
		if ($voted) {
			return $this->interactionTypeRepository->deleteWhere($input);
		}
        $this->messages[] = 'Not voted.';
        $this->error = true;
        return;
	}
	
	public function comment($input)
	{
		$interactionType = $this->interactionRepository->findWhere(['slug' => 'comment']);
		if (!$interactionType) {
			$interactionType = $this->interactionRepository->create([
				'name' => 'Comment',
				'slug' => 'comment',
				'comment' => 1
			]);
		}
		$input['interaction_type_id'] = $interactionType->id;

		$node = $this->nodeRepository->findWithoutFail($input['node_id']);
		if ($node && !$node->comment) {
            $this->messages[] = 'You can not comment this node.';
            $this->error = true;
            return;
		}

		try {
			return $this->interactionTypeRepository->create($input);
		} catch (\Exception $e) {
			$this->handleError($e);
			return;
		}
	}
	
	public function uncomment($input)
	{
		try {
			return $this->interactionTypeRepository->deleteWhere($input);		
		} catch (\Exception $e) {
			$this->handleError($e);
			return;
		}
	}
	
	public function graph($input)
	{
		try {
			return $this->graphRepository->create($input);
		} catch (\Exception $e) {
			$this->handleError($e);
			return;
		}
	}
	
	public function ungraph($input)
	{
		try {
			return $this->graphRepository->deleteWhere($input);
		} catch (\Exception $e) {
			$this->handleError($e);
			return;
		}
	}

    public function tag($input)
    {
    	try {
	        $term = $this->termRepository->firstOrCreate(['name' => $input['term'], 'slug' => str_slug($input['term'], '-')]);
	        $input['term_id'] = $term->id;
	        unset($input['term']);
    	} catch (\Exception $e) {
			$this->handleError($e);
			return;
    	}

    	try {
	        return $this->taxonomyRepository->updateOrCreate(['term_id' => $input['term_id']], $input);		
    	} catch (\Exception $e) {
			$this->handleError($e);
			return;
    	}

    }

    public function untag($input)
    {
    	try {
	        return $this->taxonomyRepository->deleteWhere($input);
    	} catch (\Exception $e) {
			$this->handleError($e);
			return;    		
    	}
    }
}
