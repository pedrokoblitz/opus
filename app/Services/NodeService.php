<?php

namespace App\Services;

use App\Repositories\NodeRepository;
use App\Repositories\AddressRepository;
use App\Repositories\EventRepository;
use App\Repositories\FileRepository;
use App\Repositories\ResourceRepository;
use App\Repositories\ContentRepository;

use App\Repositories\TermRepository;
use App\Repositories\TermTypeRepository;
use App\Repositories\TaxonomyRepository;

use App\Repositories\ParentHistoryRepository;
use App\Repositories\StatusHistoryRepository;

use Carbon\Carbon;
use Auth;
use DB;

class NodeService
{
    use ErrorHandlingTrait, SluggableTrait;

    private $locale;
    private $propertyData;

    private $currentType;
    private $currentRole;
    private $currentUser;
    private $currentApp;

    private $nodeRepository;
    private $permissionRepository;
    private $typeRepository;

    public function __construct(
        NodeRepository $nodeRepository,
        TypeRepository $typeRepository,
        PermissionRepository $permissionRepository,
    ) {
        $this->nodeRepository = $nodeRepository;
        $this->typeRepository = $typeepository;
        $this->permissionRepository = $permissionRepository;
    }

    public function all()
    {
        return $this->nodeRepository->all();
    }

    public function findWithoutFail($id)
    {
        return $this->nodeRepository->findWithoutFail($id);
    }

    public function pushCriteria($criteria)
    {
        $this->nodeRepository->pushCriteria($criteria);
    }

    public function create($input)
    {
        unset($input['node']['level']);
        unset($input['node']['type_id']);
        unset($input['node']['user_id']);

        $parentId = $input['node']['parent_id'];
        $input['node']['level'] = 0;

        if (!isset($input['node']['order'])) {
            $input['node']['order'] = 0;
        }

        $possibleStatuses = $input['type']->statuses->map(function ($item) {
            return $item->id;
        })->toArray();

        if ($parentId > 0) {
            $parent = $this->nodeRepository->findWithoutFail($parentId);
            if (!$parent) {
                $this->messages[] = 'No such parent.';
                $this->error = true;
                return;
            }
            $children_count = $parent->children->count();
            
            if ($input['node']['order'] == 0 && $children_count > 0) {
                $input['node']['order'] = $children_count;
            }

            $allowedChildrenIds = $parent->type->allowedChildren->map(function ($item) {
                return $item->id;
            })->toArray();

            if (!in_array($input['type']['id'], $allowedChildrenIds)) {
                $this->messages[] = 'Child type not allowed.';
                $this->error = true;
                return;
            }

            $input['node']['level'] = $parent->level + 1;
        }

        if ($parentId == 0) {
            $canBeRoot = $input['type']->isRoot();
            if (!$canBeRoot) {
                $this->messages[] = 'Child type not allowed.';
                $this->error = true;
                return;
            }
        }

        if (!in_array($input['node']['status_id'], $possibleStatuses)) {
            $this->messages[] = 'Status not allowed.';
            $this->error = true;
            return;
        }

        $input['node']['user_id'] = Auth::id();
        $input['node']['type_id'] = $input['type']->id;
        $slug = $this->generateSlug($input['node']['name'], $this->nodeRepository);
        $input['node']['slug'] = $slug;

        $propertySlugs = $input['type']['properties']->filter(function ($i) {
            return $i->autoload;
        })->map(function ($i) {
            return $i->slug;
        });

        DB::beginTransaction();
        try {
            $node = $this->nodeRepository->create($input['node']);
        } catch (\Exception $e) {
            DB::rollback();        
            $this->handleError($e);            
            return;
        }
        DB::commit();

        DB::beginTransaction();
        foreach ($propertySlugs as $slug) {
            if (isset($input['node'][$slug])) {
                $method = 'save' . ucfirst($slug);
                $input['node'][$slug]['node_id'] = $node->id;
                try {
                    $this->$method($input['node'][$slug]);
                } catch (\Exception $e) {
                    DB::rollback();
                    $this->handleError($e);
                    return;
                }
            }
        }
        DB::commit();

        if (isset($input['node']['terms'])) {            
            DB::beginTransaction();
            $terms = collect([]);
            foreach ($input['node']['terms'] as $term) {
                try {
                    $authorTermType = $this->termTypeRepository->findWhere(['slug' => 'author'])->first();
                    if (!$authorTermType) {
                        $authorTermType = $this->termTypeRepository
                            ->create([
                                'name' => 'Author',
                                'slug' => 'author'
                            ]);
                    }
                    $item = $this->termRepository->firstOrCreate(['name' => $term, 'slug' => \Str::slug($term, '-'), 'term_type_id' => $authorTermType->id]);
                    $terms->push($item);
                } catch (\Exception $e) {
                    DB::rollback();
                    $this->handleError($e);
                    return;
                }
            }
        
            $termIds = $terms->map(function ($i) {
                return $i->id;
            });

            try {
                $node->tags()->sync($termIds);
            } catch (\Exception $e) {
                DB::rollback();
                $this->handleError($e);
                return;
            }
            DB::commit();
        }

        return $node;
    }

    public function update($input, $id)
    {
        unset($input['node']['level']);
        unset($input['node']['parent_id']);
        unset($input['node']['type_id']);
        unset($input['node']['user_id']);

        $node = $this->nodeRepository->findWithoutFail($id);

        if (empty($node)) {
            $this->messages[] = "Node not found.";
            $this->error = true;
            return;
        }

        $possibleStatuses = $input['type']->statuses->map(function ($item) {
            return $item->id;
        })->toArray();

        if (!isset($input['node']['name']) || !$input['node']['name']) {
            $input['node']['name'] = $node->name;
        }

        if (isset($input['node']['name']) && $node->name !== $input['node']['name']) {
            $input['node']['slug'] = $this->generateSlug($input['node']['name'], $this->nodeRepository);
        }

        if (!in_array($input['node']['status_id'], $possibleStatuses)) {
            $this->messages[] = 'Status not allowed.';
            $this->error = true;
            return;
        }

        $propertySlugs = $input['type']['properties']->filter(function ($i) {
            return $i->autoload;
        })->map(function ($i) {
            return $i->slug;
        });

        DB::beginTransaction();
        try {
            $node = $this->nodeRepository->update($input['node'], $id);
        } catch (\Exception $e) {
            DB::rollback(); 
            $this->handleError($e);            
            return;
        }
        DB::commit();

        DB::beginTransaction();
        if ($node->status_id != $input['old']->status_id) {
            $statusHistory = [
                'old_status_id' => $input['old']->status_id,
                'new_status_id' => $node->status_id
            ];
            
            try {
                $this->statusHistoryRepository->create($statusHistory);
            } catch (\Exception $e) {
                DB::rollback();        
                $this->handleError($e);
                return;
            }
        } 
        DB::commit();

        foreach ($propertySlugs as $slug) {
            if (isset($input['node'][$slug])) {
                DB::beginTransaction();
                $method = 'save' . ucfirst($slug);
                $input['node'][$slug]['node_id'] = $node->id;
                try {
                    $this->$method($input['node'][$slug]);
                } catch (\Exception $e) {
                    DB::rollback();
                    $this->handleError($e);
                    return;
                }
                DB::commit();
            }
        }
        
        if (isset($input['node']['terms']))  {
            DB::beginTransaction();
            $terms = collect([]);
            foreach ($input['node']['terms'] as $term) {
                try {
                    $item = $this->termRepository->firstOrCreate(['name' => $term, 'slug' => str_slug($term, '-')]);
                    $terms->push($item);
                } catch (\Exception $e) {
                    DB::rollback();
                    $this->handleError($e);
                    return;
                }
            }
            
            $termIds = $terms->map(function ($i) {
                return $i->id;
            });

            try {
                $node->tags()->sync($termIds);
            } catch (\Exception $e) {
                DB::rollback();
                $this->handleError($e);
                return;
            }
            DB::commit();
        }
    
        return $node;
    }
    public function remove($id)
    {
        try {
            $node = $this->nodeRepository->findWithoutFail($id);
        } catch (\Exception $e) {
            $this->handleError($e);
            return;
        }

        if (!$node) {
            $this->messages[] = 'Node not found.';
            $this->error = true;
            return;            
        }

        $nodesForRemoval = collect([]);
        $this->recurseChildrenRemoval($node, $nodesForRemoval);

        $nodeIds = $nodesForRemoval->map(function($i) {
            return $i->id;
        })->toArray();
        
        foreach ($nodeIds as $nodeId) {
            $this->nodeRepository->delete($nodeId);
        }
    }

    public function trash($id, $input)
    {
        try {
            $node = $this->nodeRepository->findWithoutFail($id);
        } catch (\Exception $e) {
            $this->handleError($e);
            return;
        }

        if (!$node) {
            $this->messages[] = 'Node not found.';
            $this->error = true;
            return;            
        }

        $propertySlugs = $input['type']->properties->filter(function($i) {
            return $i->autoload;
        })->map(function ($i) {
            return $i->slug;
        })->toArray();

        $nodesForRemoval = collect([]);
        $this->recurseChildrenRemoval($node, $nodesForRemoval);

        $nodeIds = $nodesForRemoval->map(function($i) {
            return $i->id;
        })->toArray();
        
        foreach ($nodeIds as $nodeId) {
            foreach ($propertySlugs as $slug) {
                $method = 'remove' . ucfirst($slug);
                $this->$method($nodeId);
            }
            $this->nodeRepository->delete($nodeId);
        }
    }

    protected function recurseChildrenRemoval($node, &$collection) {
        $collection->push($node);
        $children = $node->children;
        if ($children && $children->count() > 0) {
            foreach ($children as $child) {
                $this->recurseChildrenRemoval($child, $collection);
            }
        }
    }

    public function changeParent($input, $id)
    {
        $parent = $this->nodeRepository->findWithoutFail($input['parent_id']);
        if (!$parent) {
            $this->messages[] = 'No such parent.';
            $this->error = true;
            return;
        }

        $allowedChildrenIds = $parent->type->allowedChildren->map(function ($item) {
            return $item->id;
        })->toArray();

        if (!in_array($input['type']['id'], $allowedChildrenIds)) {
            $this->messages[] = 'Child type not allowed.';
            $this->error = true;
            return;
        }

        DB::beginTransaction();

        try {
            $node = $this->nodeRepository->update($input, $id);
        } catch (\Exception $e) {
            DB::rollback();        
            $this->handleError($e);
            return;
        }

        if ($node->parent_id != $parent_id) {
            $parentHistory = [
                'old_parent_id' => $input['old']->parent_id,
                'new_parent_id' => $node->parent_id
            ];

            try {
                $this->parentHistoryRepository->create($parentHistory);
            } catch (\Exception $e) {
                DB::rollback();        
                $this->handleError($e);
                return;
            }
        } 
        DB::commit();

        return $node;
    }

    protected function savePersonal($input)
    {
       return $this->personalRepository->updateOrCreate(['node_id' => $input['node_id']], $input);
    }

    protected function removePersonal($node_id)
    {
       return $this->personalRepository->deleteWhere(['node_id' => $node_id]);
    }

    protected function saveAddress($input)
    {
       return $this->addressRepository->updateOrCreate(['node_id' => $input['node_id']], $input);
    }

    protected function removeAddress($node_id)
    {
        return $this->addressRepository->delete();
    }

    protected function saveEvent($input)
    {
        return $this->eventRepository->updateOrCreate(['node_id' => $input['node_id']], $input);
    }

    protected function removeEvent($node_id)
    {
        return $this->eventRepository->deleteWhere(['node_id' => $node_id]);
    }

    protected function saveFile($input)
    {
        return $this->fileRepository->updateOrCreate(['node_id' => $input['node_id']], $input);
    }

    protected function removeFile($node_id)
    {
        return $this->fileRepository->deleteWhere(['node_id' => $node_id]);
    }

    protected function saveResource($input)
    {
        return $this->resourceRepository->updateOrCreate(['node_id' => $input['node_id']], $input);
    }

    protected function removeResource($node_id)
    {
        return $this->resourceRepository->deleteWhere(['node_id' => $node_id]);
    }

    protected function saveContent($input)
    {
        if (!$input['title']) {
            $input['title'] = "Default title";
        }
        return $this->contentRepository->updateOrCreate(['node_id' => $input['node_id']], $input);
    }

    protected function removeContent($node_id)
    {
        return $this->contentRepository->deleteWhere(['node_id' => $node_id]);
    }
}