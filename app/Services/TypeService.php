<?php

namespace App\Services;

use App\Repositories\TypeRepository;
use App\Repositories\NodeRepository;
use App\Repositories\AllowedTypeHierarchyRepository;
use App\Repositories\TypeStatusRepository;
use App\Repositories\GraphRelationshipRepository;

use App\Models\App;

use DB;
use Session;

class TypeService
{
    use ErrorHandlingTrait, SluggableTrait;
    
    private $typeRepository;
    private $nodeRepository;
    private $typeStatusRepository;
    private $allowedHierarchyRepository;
    private $graphRelationshipRepository;

    public function __construct(
        TypeRepository $typeRepo,
        NodeRepository $nodeRepo,
        AllowedTypeHierarchyRepository $allowedHierarchyRepo,
        TypeStatusRepository $typeStatusRepo,
        GraphRelationshipRepository $graphRelationshipRepo
    ) {
        $this->typeRepository  = $typeRepo;
        $this->nodeRepository = $nodeRepo;
        $this->typeStatusRepository  = $typeStatusRepo;
        $this->allowedHierarchyRepository = $allowedHierarchyRepo;
        $this->graphRelationshipRepository = $graphRelationshipRepo;

        $this->app = $this->getCurrentApp();
    }

    protected function getCurrentApp()
	{
        $app_id = Session::get('app_id'); 
        if ($app_id) {
            return App::where('id', $app_id)->first();
        }
        if (!$app_id) {
            return App::where('default', 1)->first();
        }
    }
    
    public function all()
    {
        $with = ['properties', 'statuses', 'allowedChildren'];
        // $with = ['properties'];
        try {
            return $this->typeRepository
                ->with($with)
                ->findWhere(['app_id' => $this->app->id]);
        } catch (Exception $e) {
            $this->handleError($e);
            return null;
        }
    }

    public function findWithoutFail($id)
    {
        $with = ['properties', 'statuses', 'allowedChildren'];
        try {
            return $this->typeRepository
                ->with($with)
                ->findWhere(['id' => $id, 'app_id' => $this->app->id]);
        } catch (\Exception $e) {
            $this->handleError($e);
            return null;
        }
    }

    public function findBySlugWithoutFail($slug)
    {
        $with = ['properties', 'statuses', 'allowedChildren'];
        try {
            return $this->typeRepository
                ->with($with)
                ->findWhere(['slug' => $slug, 'app_id' => $this->app->id]);
        } catch (\Exception $e) {
            $this->handleError($e);
            return null;
        }
    }

    protected function checkParent($input)
    {

    }
    
    public function create($input)
    {
        DB::beginTransaction();

        $properties = isset($input['properties']) ? $input['properties'] : [];
        $statuses = isset($input['statuses']) ? $input['statuses'] : [];
        $allowedChildren = isset($input['allowed_children']) ? $input['allowed_children'] : [];
        $allowedParents = isset($input['allowed_parents']) ? $input['allowed_parents'] : [];
        $relationships = isset($input['relationships']) ? $input['relationships'] : [];

        unset($input['properties']);
        unset($input['statuses']);
        unset($input['allowed_children']);
        unset($input['allowed_parents']);
        unset($input['relationships']);

        $input['slug'] = $this->generateSlug($input['name'], $this->typeRepository);

        try {
            $type = $this->typeRepository->create($input);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->handleError($e);            
            return;
        }

        try {
            $type->properties()->sync($properties);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->handleError($e);            
            return;
        }

        foreach ($statuses as $status) {
            try {
                $status['type_id'] = $type->id;
                $status = $this->typeStatusRepository
                    ->create($status)
                    ->toArray();
            } catch (\Exception $e) {
                DB::rollBack();
                $this->handleError($e);            
                return;
            }
        }

        try {
            $type->allowedParents()->sync($allowedParents);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->handleError($e);            
            return;
        }

        $relationshipIds = [];
        foreach ($relationships as $relationship) {
            $relationship['source_type_id'] = $type->id;
            $relationship['slug'] = $this->generateSlug($relationship['name'], $this->graphRelationshipRepository);
            $relationshipIds[] = $this->graphRelationshipRepository->create($relationship)->id;
        }

        DB::commit();
        return $type;
    }

    public function update($input, $id)
    {
        $type = $this->typeRepository->findWithoutFail($id);
        if (!$type) {
            $this->error = true;
            $this->messages[] = 'Type not found.';
            return;
        }

        $hasNodes = $this->nodeRepository->findWhere(['type_id' => $id])->count();
        if ($hasNodes) {
            $this->error = true;
            $this->messages[] = 'Type has nodes.';
            return;
        }

        if (isset($input['name']) && $type->name !== $input['name']) {
            $input['name'] = $this->generateSlug($input['name'], $this->typeRepository);
        }

        DB::beginTransaction();
        $relationships = isset($input['relationships']) ? $input['relationships'] : [];
        $properties = isset($input['properties']) ? $input['properties'] : [];
        $statuses = isset($input['statuses']) ? $input['statuses'] : [];
        $allowedChildren = isset($input['allowed_children']) ? $input['allowed_children'] : [];
        $allowedParents = isset($input['allowed_parents']) ? $input['allowed_parents'] : [];

        unset($input['app_id']);
        unset($input['properties']);
        unset($input['statuses']);
        unset($input['relationships']);
        unset($input['allowed_children']);
        unset($input['allowed_parents']);

        try {
            $type = $this->typeRepository->update($input, $id);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->handleError($e);            
            return;
        }

        try {
            $type->properties()->sync($properties);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->handleError($e);            
            return;
        }

        if (!isset($statuses) || !$statuses) {
            $statuses = [];
        } elseif (count($statuses) > 0) {
            $oldStatusIds = $type->statuses->map(function ($i) {
                return $i->id;
            })->toArray();
    
            $newStatusIds = collect($statuses)->filter(function ($i) {
                return isset($i->id) && $i->id > 0;
            })->map(function ($i) {
                return $i->id;
            })->toArray();
    
            foreach ($oldStatusIds as $oldStatusId) {
                if (!in_array($oldStatusId, $newStatusIds)) {
                    $this->typeStatusRepository->delete($oldStatusId);
                }
            }
    
            foreach ($statuses as $status) {
                try {
                    $hasId = isset($status['id']) && $status['id'] > 0;
                    if (!$hasId) {
                        $status['type_id'] = $type->id;
                        $status = $this->typeStatusRepository
                            ->create($status)
                            ->toArray();
                    }

                    if ($hasId) {
                        $this->typeStatusRepository->update($status, $status['id']);
                    }
                } catch (\Exception $e) {
                    DB::rollBack();
                    $this->handleError($e);
                    return;
                }
            }
        }

        try {
            $type->allowedParents()->sync($allowedParents);
        } catch (\Exception $e) {
            DB::rollBack();
            $this->handleError($e);            
            return;
        }

        $oldRelationshipIds = $type->targets->map(function ($i) {
            return $i->id;
        })->toArray();

        $newRelationshipIds = collect($relationships)->filter(function ($i) {
            return isset($i->id) && $i->id > 0;
        })->map(function ($i) {
            return $i->id;
        })->toArray();

        foreach ($oldRelationshipIds as $oldRelationshipId) {
            try {
                if (!in_array($oldRelationshipId, $newRelationshipIds)) {
                    $this->graphRelationshipRepository->deleteWhere([
                        'target_type_id' => $oldRelationshipId,
                        'source_type_id' => $type->id
                    ]);
                }
            } catch (Exception $e) {
                DB::rollBack();
                $this->handleError($e);            
            }
        }

        foreach ($relationships as $relationship) {
            $relationship['source_type_id'] = $type->id;
            $hasId = isset($relationship['id']) && $relationship['id'];
            try {
                if (!$hasId) {
                    $relationship['slug'] = $this->generateSlug($relationship['name'], $this->graphRelationshipRepository);
                    $this->graphRelationshipRepository->create($relationship)->id;
                }
                if ($hasId) {
                    $this->graphRelationshipRepository->update($relationship, $relationship['id']);
                }
            } catch (Exception $e) {
                DB::rollBack();
                $this->handleError($e);            
            }
        }
        if ($this->isError()) {
            return;
        }        
        DB::commit();
        return $type;
    }

    public function delete($id)
    {
        $hasNodes = $this->nodeRepository
            ->where(['type_id', $id])
            ->count();

        if ($hasNodes) {
            $this->error = true;
            $this->messages[] = 'Type has nodes.';
            return;
        }

        return $this->typeRepository->delete($id);
    }
}
