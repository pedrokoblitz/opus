<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateBlogAppTest extends TestCase
{
    public function testCreateApp()
    {
        $payload = [
          'name' => 'Test',
          'slug' => 'test',
          'default' => 1
        ];
        $response = $this->post('/api/app', $payload);
        $response->assertStatus(200);
    }

    public function testCreateTypes()
    {
      $app = \DB::table('apps')->where('default', 1)->first();
      $payload = [
        'type' => [
          'app_id' => $app->id,
          'name' => 'Post',
          'slug' => 'post',
          'statuses' => [
            [
              'status' => 'draft',
              'default' => 0,
            ],
            [
              'status' => 'published',
              'default' => 1,
            ],
          ],
          'properties' => [1,2,3,5],
        ],
      ];
      $response = $this->post('/api/type', $payload);
      $response->assertStatus(200);
      $post = json_decode($response->getContent());

      $payload = [
        'type' => [
          'app_id' => $app->id,
          'name' => 'Page',
          'slug' => 'page',
          'statuses' => [
            [
              'status' => 'draft',
              'default' => 0,
            ],
            [
              'status' => 'published',
              'default' => 1,
            ],
          ],
          'properties' => [1,2,3,5],
        ],
      ];
      $response = $this->post('/api/type', $payload);
      $response->assertStatus(200);
      $page = json_decode($response->getContent());
      
      $payload = [
        'type' => [
          'app_id' => $app->id,
          'name' => 'Blog',
          'slug' => 'blog',
          'statuses' => [
            [
              'status' => 'private',
              'default' => 1,
            ],
          ],
          'relationships' => [
            [
              'name' => 'Referência',
              'target_type_id' => $page->data->id
            ]
          ],
          'properties' => [1,2,3,5],
        ],
      ];
      $response = $this->post('/api/type', $payload);
      $response->assertStatus(200);
      $blog = json_decode($response->getContent());

      $payload = [
        'type' => [
          'allowed_parents' => [3],
        ],
      ];
      $response = $this->put('/api/type/' . $post->data->id, $payload);
      $response->assertStatus(200);

      $payload = [
        'type' => [
          'allowed_parents' => [3,2],
        ]
      ];
      $response = $this->put('/api/type/' . $page->data->id, $payload);
      $response->assertStatus(200);

      $payload = [
        'type' => [
          'allowed_parents' => [0],
          'statuses' => [
            [
              'status' => 'private',
              'default' => 0,
            ],
            [
              'status' => 'public',
              'default' => 1,
            ],
          ],
          'relationships' => [
            [
              'name' => 'Teste',
              'target_type_id' => $page->data->id
            ]
          ],  
        ]
      ];
      $response->assertStatus(200);
      $response = $this->put('/api/type/' . $blog->data->id, $payload);
    }

    public function testCreateRoles()
    {
      $app = \DB::table('apps')->where('default', 1)->first();

      $payload = [
        'role' => [
          'app_id' => $app->id,
          'name' => 'Editor',
          'permissions' => [
            [
              'type_id' => 1,
              'action' => 'CREATE',
            ],
            [
              'type_id' => 1,
              'action' => 'READ',
            ],
            [
              'type_id' => 1,
              'action' => 'UPDATE',
            ],
            [
              'type_id' => 1,
              'action' => 'DELETE',
            ],
            [
              'type_id' => 2,
              'action' => 'CREATE',
            ],
            [
              'type_id' => 2,
              'action' => 'READ',
            ],
            [
              'type_id' => 2,
              'action' => 'UPDATE',
            ],
            [
              'type_id' => 2,
              'action' => 'DELETE',
            ],
            [
              'type_id' => 3,
              'action' => 'CREATE',
            ],
            [
              'type_id' => 3,
              'action' => 'READ',
            ],
            [
              'type_id' => 3,
              'action' => 'UPDATE',
            ],
            [
              'type_id' => 3,
              'action' => 'DELETE',
            ],
          ],
        ],
      ];

      $response = $this->post('/api/role', $payload);
      $response->assertStatus(200);
    }

    public function testCreateUsers()
    {
      $payload = [
        'user' => [
          'email' => 'exemplo@teste.com',
          'password' => '1234',
          'personal' => [
            'fullname' => 'Jose da Silva',
            'birthdate' => '1999-01-01',
          ],
          'role' => 3,
        ],
      ];

      $response = $this->post('/api/user', $payload);
      $response->assertStatus(200);
    }
}
