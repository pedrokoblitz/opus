<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CreateRecordsTest extends TestCase
{
  use AuthenticableTrait;

  public function testCreateBlog()
  {
    $this->auth();
    $payload = [
      'node' => [
        'name' => 'Meu blog',
        'parent_id' => 0,
        'status_id' => 7,
      ],
    ];
    $response = $this->post('/api/node/blog', $payload);
    $response->assertStatus(200);
  }

  public function testCreatePages()
  {
    $this->auth();
    $payload = [
      'node' => [
        'name' => 'pagina',
        'parent_id' => 2,
        'status_id' => 4,
        'terms' => ['uma tag exemplo'],
      ],
    ];
    $response = $this->post('/api/node/page', $payload);
    $response->assertStatus(200);

    $payload = [
      'node' => [
        'name' => 'sub pagina',
        'parent_id' => 3,
        'status_id' => 3,
        'terms' => ['uma tag exemplo', 'tag'],
      ],
    ];
    $response = $this->post('/api/node/page', $payload);
    $response->assertStatus(200);
  }

  public function testCreatePosts()
  {
    $this->auth();
    $payload = [
      'node' => [
        'name' => 'Testando',
        'parent_id' => 2,
        'status_id' => 2,
        'terms' => ['uma tag exemplo', 'exemplo'],
      ],
    ];
    $response = $this->post('/api/node/post', $payload);
    $response->assertStatus(200);
  }
}
