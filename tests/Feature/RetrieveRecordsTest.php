<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RetrieveRecordsTest extends TestCase
{
    use AuthenticableTrait;

    public function testShowBlog()
    {
        $this->auth();
        $response = $this->get('/api/node/blog/2');
        $response->assertStatus(200);
    }

    public function testShowPost()
    {
        $this->auth();
        $response = $this->get('/api/node/post/5');
        $response->assertStatus(200);
    }

    public function testIndexPosts()
    {
        $this->auth();
        $response = $this->get('/api/node/post');
        $response->assertStatus(200);
    }
}
