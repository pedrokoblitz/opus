<?php

namespace Tests\Feature;

trait AuthenticableTrait
{
    public function auth()
    {
        $this->post('/api/auth', []);
    }
}
